import NextI18NextInstance from '../../i18n'

export const {
  appWithTranslation,
  withTranslation
} = NextI18NextInstance

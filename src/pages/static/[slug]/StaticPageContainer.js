import React from 'react'
import { prop } from 'ramda'
import { sprintf } from 'sprintf-js'
import * as API from 'constants/api'
import fetchData from 'helpers/fetchData'
import StaticPage from './StaticPage'

const StaticPageContainer = props => (
  <StaticPage {...props} />
)

StaticPageContainer.getInitialProps = async ({ query }) => {
  const slug = prop('slug', query)
  const data = await fetchData(sprintf(API.STATIC_PAGE, slug))

  return {
    data,
    namespacesRequired: ['common']
  }
}

export default StaticPageContainer

import React from 'react'
import PropTypes from 'prop-types'
import * as ROUTES from 'constants/routes'
import getTranslate from 'helpers/getTranslate'
import { withTranslation } from 'hocs/withTranslation'
import DocumentTitle from 'components/DocumentTitle'
import { DetailLayout } from 'components/Layouts'
import { Breadcrumb, BreadcrumbItem } from 'components/Breadcrumb'
import Title from 'components/Title'
import HtmlContent from 'components/HtmlContent'

const StaticPage = props => {
  const { t, data } = props

  const title = getTranslate(data)
  const content = getTranslate(data, 'body')

  return (
    <DetailLayout>
      <DocumentTitle>{title}</DocumentTitle>

      <Breadcrumb>
        <BreadcrumbItem href={ROUTES.ROOT_PATH}>{t('nav_home')}</BreadcrumbItem>
        <BreadcrumbItem>{title}</BreadcrumbItem>
      </Breadcrumb>

      <Title>{title}</Title>
      <HtmlContent>{content}</HtmlContent>
    </DetailLayout>
  )
}

StaticPage.propTypes = {
  t: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
}

export default withTranslation()(StaticPage)

import React from 'react'
import PropTypes from 'prop-types'
import { head, prop } from 'ramda'
import * as ROUTES from 'constants/routes'
import { withTranslation } from 'hocs/withTranslation'
import DocumentTitle from 'components/DocumentTitle'
import { BaseLayout } from 'components/Layouts'
import { Breadcrumb, BreadcrumbItem } from 'components/Breadcrumb'
import { TitleBig } from 'components/Title'
import Pagination from 'components/Pagination'
import { NewsGrid, NewsBigCard, NewsCard } from 'components/News'

const News = props => {
  const { t, newsData } = props

  const list = prop('results', newsData) || []
  const mainNews = head(list)
  const count = prop('count', newsData)

  return (
    <BaseLayout>
      <DocumentTitle>{t('news_title')}</DocumentTitle>

      <Breadcrumb>
        <BreadcrumbItem href={ROUTES.ROOT_PATH}>{t('nav_home')}</BreadcrumbItem>
        <BreadcrumbItem>{t('news_title')}</BreadcrumbItem>
      </Breadcrumb>

      <TitleBig>{t('news_title')}</TitleBig>

      <NewsBigCard data={mainNews} />

      <NewsGrid>
        {list.map(item => (
          <NewsCard key={item.id} data={item} />
        ))}
      </NewsGrid>

      <Pagination
        totalRecords={count}
        pageLimit={12}
      />
    </BaseLayout>
  )
}

News.propTypes = {
  t: PropTypes.func.isRequired,
  newsData: PropTypes.object.isRequired,
}

export default withTranslation()(News)

import React from 'react'
import PropTypes from 'prop-types'
import { prop } from 'ramda'
import styled from 'styled-components'
import * as ROUTES from 'constants/routes'
import getTranslate from 'helpers/getTranslate'
import { withTranslation } from 'hocs/withTranslation'
import DocumentTitle from 'components/DocumentTitle'
import { DetailLayout } from 'components/Layouts'
import { Breadcrumb, BreadcrumbItem } from 'components/Breadcrumb'
import Title from 'components/Title'
import { Datee } from 'components/StyledElements'
import HtmlContent from 'components/HtmlContent'

const StyledTitle = styled(Title)`
  margin-bottom: 15px;
`

const Image = styled('img')`
  display: block;
  margin: 30px 0;
  max-width: 100%;
`

const Content = styled(HtmlContent)`
  font-size: 16px;
`

const NewsDetail = props => {
  const { t, data } = props

  const title = getTranslate(data, 'title')
  const content = getTranslate(data, 'text')
  const createdDate = prop('created_date_by_admin', data)
  const image = prop('image_main', data)

  return (
    <DetailLayout>
      <DocumentTitle>{title}</DocumentTitle>

      <Breadcrumb>
        <BreadcrumbItem href={ROUTES.ROOT_PATH}>{t('nav_home')}</BreadcrumbItem>
        <BreadcrumbItem href={ROUTES.NEWS_PATH}>{t('news_title')}</BreadcrumbItem>
        <BreadcrumbItem>{title}</BreadcrumbItem>
      </Breadcrumb>

      <StyledTitle>{title}</StyledTitle>
      <Datee>{createdDate}</Datee>
      <Image src={image} alt={title} />
      <Content>{content}</Content>
    </DetailLayout>
  )
}

NewsDetail.propTypes = {
  t: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired
}

export default withTranslation()(NewsDetail)

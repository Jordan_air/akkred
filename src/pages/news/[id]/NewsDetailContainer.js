import React from 'react'
import { sprintf } from 'sprintf-js'
import * as API from 'constants/api'
import fetchData from 'helpers/fetchData'
import NewsDetail from './NewsDetail'
import { prop } from 'ramda'

const NewsDetailContainer = props => (
  <NewsDetail {...props} />
)

NewsDetailContainer.getInitialProps = async ({ query }) => {
  const id = prop('id', query)
  const data = await fetchData(sprintf(API.NEWS_ITEM, id))

  return {
    data,
    namespacesRequired: ['common']
  }
}

export default NewsDetailContainer

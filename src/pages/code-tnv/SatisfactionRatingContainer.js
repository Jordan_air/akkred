import React, { useState } from 'react'
import { not, path, pipe } from 'ramda'
import { useRouter } from 'next/router'
import { useToasts } from 'react-toast-notifications'
import * as API from 'constants/api'
import { mapResponseToFormError } from 'helpers/form'
import SatisfactionRating from './SatisfactionRating'

const serializer = values => ({
  ...values,
})

const SatisfactionRatingContainer = props => {
  const [loading, setLoading] = useState(false)
  const [result, setResult] = useState(null)
  const { addToast } = useToasts()

  async function onSubmit (values) {
    setLoading(true)

    const serializedValues = serializer(values)
    const body = JSON.stringify(serializedValues)
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      },
      body
    }
    const data = await fetch(API.CODE_TNV_LIST, options)
      .then(response => {
        setLoading(false)

        if (not(response.ok)) {
          return Promise.reject(response.json())
        }

        return response.json()
      })
      .then(response => {
        setResult({
          data: response,
          values: serializedValues
        })
      })
      .catch(response => {
        addToast('Ошибка', { appearance: 'error' })
        setResult(null)
        return response.then(mapResponseToFormError)
      })

    return data
  }

  return (
    <SatisfactionRating
      onSubmit={onSubmit}
      result={result}
      loading={loading}
      {...props}
    />
  )
}

SatisfactionRatingContainer.getInitialProps = async () => ({
  namespacesRequired: ['common']
})

export default SatisfactionRatingContainer

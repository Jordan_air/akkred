import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import * as ROUTES from 'constants/routes'
import { withTranslation } from 'hocs/withTranslation'
import DocumentTitle from 'components/DocumentTitle'
import { DetailLayout } from 'components/Layouts'
import { Breadcrumb, BreadcrumbItem } from 'components/Breadcrumb'
import Title from 'components/Title'
import {
  Form,
  Field,
  InputField,
  UniversalSelectField,
} from 'components/FormComponents'
import { FormWrapper, FieldWrap } from 'components/StyledElements'
import { ButtonPrimary } from 'components/Button'
import * as API from '../../constants/api'
import numberFormat from '../../helpers/numberFormat'
import { Table, TableBody, TableCol, TableHeader, TableRow } from '../../components/Table'
import { path, prop } from 'ramda'
import { RegistryDetail, RegistryRow } from '../../components/Registry'
import styled from 'styled-components'
import { API_URL } from '../../constants/api'

const TableContainer = styled('div')`
  text-align: center;
  margin-top: 10px;
`

const ClickableRow = styled(RegistryRow)`
  cursor: pointer;
`

const SatisfactionRating = props => {
  const {
    t,
    onSubmit,
    loading,
    result
  } = props
  const reestr = path(['data', 'results'], result)
  const tnv = path(['data', 'result'], result)

  return (
    <DetailLayout>
      <DocumentTitle>{t('tnv_title')}</DocumentTitle>

      <Breadcrumb>
        <BreadcrumbItem href={ROUTES.ROOT_PATH}>{t('nav_home')}</BreadcrumbItem>
        <BreadcrumbItem>{t('tnv_title')}</BreadcrumbItem>
      </Breadcrumb>

      <Title>{t('tnv_title')}</Title>

      <Form
        onSubmit={onSubmit}
        resetOnSuccess={true}
        keepDirtyOnReinitialize={false}>
        <Fragment>
          <FormWrapper>

            <FieldWrap>
              <Field
                name={'code'}
                component={InputField}
                label={t('tnv_code')}
                type={'number'}
              />
            </FieldWrap>

            <FieldWrap>
              <Field
                name={'inn'}
                component={InputField}
                label={t('tnv_inn')}
                type={'number'}
              />
            </FieldWrap>

            <FieldWrap>
              <Field
                name={'name'}
                component={InputField}
                label={t('tnv_name')}
              />
            </FieldWrap>

            <FieldWrap>
              <ButtonPrimary loading={loading}>
                Отправить запрос
              </ButtonPrimary>
            </FieldWrap>
          </FormWrapper>
        </Fragment>
      </Form>

      {reestr && (
        <TableContainer>
          <Table gutter={40}>
            <TableHeader>
              <TableRow>
                <TableCol span={6}>{t('registry_table_number')}</TableCol>
                <TableCol span={6}>{t('registry_table_legal')}</TableCol>
                <TableCol span={6}>{t('registry_table_expiration')}</TableCol>
                <TableCol span={6}>{t('registry_status')}</TableCol>
              </TableRow>
            </TableHeader>
            <TableBody>

              {reestr.map(item => {
                const id = prop('id', item)

                return (
                  <ClickableRow
                    t={t}
                    key={id}
                    data={item}
                  />
                )
              })}
            </TableBody>
          </Table>
        </TableContainer>
      )}

      {tnv && (
        <div style={{
          marginTop: '20px'
        }}>
          <p>
            Murojaat qilingan vaqt holatiga <strong> {tnv.code} </strong>TIF TN kodiga mansub mahsulotlarni sertifikatlashtirish
            faoliyatini amalga oshirayotgan sertifikatlashtirish organi “Akkreditatsiya qilingan muvofiqlikni baholash
            organlari davlat reestri”da mavjud emas.
          </p> <br/>
          <a href={`${API_URL}/main/tnvs/${tnv.id}/pdf`}>{t('tnv_document')}</a>

        </div>
      )}
    </DetailLayout>
  )
}

SatisfactionRating.propTypes = {
  t: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired
}
SatisfactionRating.defaultProps = {
  reestr: [],
}

export default withTranslation()(SatisfactionRating)

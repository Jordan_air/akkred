/* eslint-disable max-len */
import React from 'react'
import Document, { Html, Head, Main, NextScript } from 'next/document'
import { ServerStyleSheet } from 'styled-components'
import { LIVE_CHAT_SCRIPT,YANDEX_METRIC } from 'constants/backend'

export default class MyDocument extends Document {
  static getInitialProps ({ renderPage }) {
    const sheet = new ServerStyleSheet()

    const page = renderPage((App) => (props) =>
      sheet.collectStyles(<App {...props} />)
    )

    const styleTags = sheet.getStyleElement()

    return { ...page, styleTags }
  }

  render () {
    return (
      <Html>
        <Head>
          <title>Akkred.uz</title>
          <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500&display=swap&subset=cyrillic" rel="stylesheet" />
          <link rel="shortcut icon" href="/favicon.ico" />
          {this.props.styleTags}
        </Head>

        <body>
          <Main />
          <NextScript />

          <script dangerouslySetInnerHTML={{ __html: LIVE_CHAT_SCRIPT }}/>
          <script dangerouslySetInnerHTML={{ __html: YANDEX_METRIC }}/>
        </body>
      </Html>
    )
  }
}

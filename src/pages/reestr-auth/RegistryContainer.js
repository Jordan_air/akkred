import React, { useEffect, useState } from 'react'
import { filter, map, pick, pipe, propOr, split } from 'ramda'
import { useRouter } from 'next/router'
import * as API from 'constants/api'
import fetchData from 'helpers/fetchData'
import { onFormChange, onFormReset } from 'components/RegistryAuth'
import Registry from './Registry'

const fieldNames = [
  'typeOrgan',
  'status',
  'region',
  'searchNumber',
  'searchTitle',
  'searchCode',
  'searchInn',
  'search',
]

const getMultiFieldInitValue = (query, name, isStatic) => pipe(
  propOr('', name),
  split('-'),
  map(id => {
    if (id) {
      return {
        id: isStatic ? id : Number(id)
      }
    }
    return null
  }),
  filter(Boolean)
)(query)

const RegistryContainer = props => {
  const router = useRouter()

  const { query } = router

  const typeOrgan = getMultiFieldInitValue(query, 'typeOrgan')
  const region = getMultiFieldInitValue(query, 'region')
  const status = getMultiFieldInitValue(query, 'status', true)

  const initialValues = {
    ...pick(fieldNames, query),
    typeOrgan,
    region,
    status
  }

  const [regionData, setRegionData] = useState({})
  const [organTypeData, setOrganTypeData] = useState({})

  useEffect(() => {
    const defaultParams = { pageSize: 100 }

    fetchData(API.REGION_LIST, defaultParams).then(setRegionData)
    fetchData(API.TYPE_ORGAN_LIST, defaultParams).then(setOrganTypeData)
  }, [])

  const onChange = (value, isForced) => onFormChange(value, router, isForced)
  const onReset = () => onFormReset(router, initialValues)

  return (
    // <div>
    //   404
    // </div>
    <Registry
      regionData={regionData}
      organTypeData={organTypeData}
      onFormChange={onChange}
      onFormReset={onReset}
      initialValues={initialValues}
      {...props}
    />
  )
}

RegistryContainer.getInitialProps = async ({ query }) => {
  const pickParams = [...fieldNames, 'page']
  const params = pick(pickParams, query)
  const registryData = await fetchData(API.REGISTRY_AUTH_LIST, params)

  return {
    registryData,
    namespacesRequired: ['common']
  }
}

export default RegistryContainer

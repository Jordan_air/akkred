import React from 'react'
import { pick } from 'ramda'
import { useRouter } from 'next/router'
import * as API from 'constants/api'
import fetchData from 'helpers/fetchData'
import { onFormChange, onFormReset } from 'components/Registry'
import RegistryInfo from './RegistryInfo'

const fieldNames = [
  'status',
  'searchNumber',
  'searchTitle'
]

const RegistryInfoContainer = props => {
  const router = useRouter()

  const { query } = router

  const initialValues = pick(fieldNames, query)

  const onChange = (values, isForced) => onFormChange(values, router, isForced)
  const onReset = () => onFormReset(router, initialValues)

  return (
    <RegistryInfo
      onFormReset={onReset}
      onFormChange={onChange}
      initialValues={initialValues}
      {...props}
    />
  )
}

RegistryInfoContainer.getInitialProps = async ({ query }) => {
  const data = await fetchData('https://e.akkred.uz:8081/integration_api/application/', query)

  return {
    data,
    namespacesRequired: ['common']
  }
}

export default RegistryInfoContainer

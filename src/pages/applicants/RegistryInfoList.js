import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { prop, path } from 'ramda'
import * as CONST from 'constants/backend'
import styled from 'styled-components'
import dateFormat from 'helpers/dateFormat'
import {
  Table,
  TableHeader,
  TableBody,
  TableRow,
  TableCol
} from 'components/Table'
import { applicationStatusStatus } from 'constants/backend'

const ListCounts = styled('div')`
  font-size: 18px;
  margin-top: 25px;
`
const Count = styled('span')`
  color: ${props => props.theme.colors.grey4};
  font-weight: 500;
`
const TableContainer = styled('div')`
  text-align: start;
`

const RegistryInfoList = props => {
  const { t, data, count} = props

  return (
    <Fragment>
      <TableContainer>
        <Table>
          <TableHeader>
            <TableRow>
              <TableCol span={8}>{t("applicant")}</TableCol>
              <TableCol span={6}>{t("response_person")}</TableCol>
              <TableCol span={4} >{t("date_confirm")}</TableCol>
              <TableCol span={6} align={'right'}>{t("registry_info_status")}</TableCol>

            </TableRow>
          </TableHeader>
          <TableBody>
            {data.map(item => {
              const id = prop('id', item)
              const client = prop('client', item)
              const stage = prop('stage', item)
              const registerDate = dateFormat(prop('register_date', item))
              const objectFullName = prop('object_full_name', client)
              const executor = prop('executor', item)
              const firstName = prop('first_name', executor)
              const lastName = prop('last_name', executor)
              const middleName = prop('middle_name', executor)
              const stageName = CONST.applicationStatusStatus.object[stage]

              return (
                <TableRow key={id}>
                  <TableCol span={8}>{objectFullName}</TableCol>
                  <TableCol span={6}>{firstName} {lastName} {middleName}</TableCol>
                  <TableCol span={4} >{registerDate}</TableCol>
                  <TableCol span={6} align={'right'}>{stageName}</TableCol>

                </TableRow>
              )
            })}
          </TableBody>
        </Table>
      </TableContainer>
      <ListCounts>
        {t('registry_query_count')}: <Count>{count}</Count>
      </ListCounts>
    </Fragment>
  )
}

RegistryInfoList.propTypes = {
  t: PropTypes.func.isRequired,
  data: PropTypes.array.isRequired,
}

RegistryInfoList.defaultProps = {
  data: [],
  count: 0,
}

export default RegistryInfoList

import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { always, filter, isEmpty, not, pipe, prop } from 'ramda'
import { Search } from 'react-feather'
import * as CONST from 'constants/backend'
import { RegistryFilterWrapper } from 'components/Registry'
import {
  Form,
  Field,
  AutoSave,
  IconInputField,
  RadioGroup
} from 'components/FormComponents'
import { Radio } from 'components/Toggle/Radio'
import { FieldWrap } from 'components/StyledElements'
import { ButtonBordered } from 'components/Button'

const RegistryInfoFilter = props => {
  const {
    t,
    initialValues,
    onFormChange,
    onFormReset
  } = props

  const onEnterSearch = (event, fieldValue) => {
    const fieldName = event.target.name

    event.preventDefault()

    onFormChange({ [fieldName]: fieldValue }, true)
  }

  const isFilterApplied = pipe(
    Object.values,
    filter(Boolean),
    isEmpty,
    not
  )(initialValues)

  const statusFieldList = CONST.registryInfoStatus.list.map(item => {
    const id = prop('id', item)
    const title = prop('title', item)

    return (
      <Radio
        key={id}
        value={id}
        label={t(title)}
      />
    )
  })

  return (
    <RegistryFilterWrapper>
      <Form initialValues={initialValues} onSubmit={always(true)}>
        <Fragment>
          <AutoSave
            debounce={300}
            onFormChange={onFormChange}
          />

          {/*<FieldWrap>*/}
          {/*  <ButtonBordered*/}
          {/*    disabled={not(isFilterApplied)}*/}
          {/*    onClick={onFormReset}*/}
          {/*    fullWidth={true}>*/}
          {/*    {t('registry_reset_form')}*/}
          {/*  </ButtonBordered>*/}
          {/*</FieldWrap>*/}

          <FieldWrap>
            <Field
              name={'status'}
              component={RadioGroup}
              label={t('registry_info_status')}>
              {statusFieldList}
            </Field>
          </FieldWrap>

          <FieldWrap>
            <Field
              name={'searchNumber'}
              component={IconInputField}
              label={t('registry_number')}
              placeholder={t('search')}
              icon={Search}
              onEnter={onEnterSearch}
            />
          </FieldWrap>

          <FieldWrap>
            <Field
              name={'searchTitle'}
              component={IconInputField}
              label={t('registry_legal_title')}
              placeholder={t('search')}
              icon={Search}
              onEnter={onEnterSearch}
            />
          </FieldWrap>
        </Fragment>
      </Form>
    </RegistryFilterWrapper>
  )
}

RegistryInfoFilter.propTypes = {
  t: PropTypes.func.isRequired,
  initialValues: PropTypes.object.isRequired,
  onFormChange: PropTypes.func.isRequired,
  onFormReset: PropTypes.func.isRequired
}

export default RegistryInfoFilter

import React from 'react'
import PropTypes from 'prop-types'
import { prop } from 'ramda'
import styled from 'styled-components'
import * as ROUTES from 'constants/routes'
import { withTranslation } from 'hocs/withTranslation'
import DocumentTitle from 'components/DocumentTitle'
import { BaseLayout } from 'components/Layouts'
import { Breadcrumb, BreadcrumbItem } from 'components/Breadcrumb'
import { Row, Col } from 'components/Grid'
import { RegistryDetail as Detail } from 'components/RegistryConfirm'
import Title from 'components/Title'
import { Datee } from 'components/StyledElements'

const Header = styled('div')`
  margin-bottom: 25px;
`

const StyledTitle = styled(Title)`
  margin-bottom: 15px;
`

const RegistryDetail = props => {
  const { t, data } = props

  const title = prop('title_organ', data)
  const accreditationDuration = prop('accreditation_duration', data)

  return (
    <BaseLayout>
      <DocumentTitle>{title}</DocumentTitle>

      <Row>
        <Col span={16}>
          <Breadcrumb>
            <BreadcrumbItem href={ROUTES.ROOT_PATH}>{t('nav_home')}</BreadcrumbItem>
            <BreadcrumbItem href={ROUTES.REGISTRY_CONFIRM_PATH}>
              {'Texnik jihatdan malakaliligi maʼqullangan laboratoriyalar reestri'}</BreadcrumbItem>
            <BreadcrumbItem>{title}</BreadcrumbItem>
          </Breadcrumb>

          <Header>
            <StyledTitle>{title}</StyledTitle>
            {accreditationDuration && (
              <Datee>{accreditationDuration}</Datee>
            )}
          </Header>

          <Detail t={t} data={data} />
        </Col>
      </Row>
    </BaseLayout>
  )
}

RegistryDetail.propTypes = {
  t: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired
}

export default withTranslation()(RegistryDetail)

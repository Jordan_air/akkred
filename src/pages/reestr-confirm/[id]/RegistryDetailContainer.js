import React from 'react'
import * as API from 'constants/api'
import fetchData from 'helpers/fetchData'
import RegistryDetail from './RegistryDetail'
import { prop } from 'ramda'
import { sprintf } from 'sprintf-js'

const RegistryDetailContainer = props => (
  <RegistryDetail {...props} />
)

RegistryDetailContainer.getInitialProps = async ({ query }) => {
  const id = prop('id', query)
  const data = await fetchData(sprintf(API.REGISTRY_CONFIRM_ITEM, id))

  return {
    data,
    namespacesRequired: ['common']
  }
}

export default RegistryDetailContainer

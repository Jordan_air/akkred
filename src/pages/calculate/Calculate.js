import React from 'react'
import PropTypes from 'prop-types'
import { prop } from 'ramda'
import { sprintf } from 'sprintf-js'
import styled from 'styled-components'
import * as ROUTES from 'constants/routes'
import * as CONST from 'constants/backend'
import { withTranslation } from 'hocs/withTranslation'
import DocumentTitle from 'components/DocumentTitle'
import { DetailLayout } from 'components/Layouts'
import { Breadcrumb, BreadcrumbItem } from 'components/Breadcrumb'
import Title from 'components/Title'
import Link from 'components/Link'

const Block = styled('div')`
  
`

const SubTitle = styled('div')`
  font-size: 20px;
  font-weight: 500;
  margin-bottom: 16px;
`

const StyledLink = styled(Link)`
  display: block;
  line-height: 20px;
  &:not(:last-child) {
    margin-bottom: 8px;
  }
`

const DisabledText = styled('div')`
  line-height: 20px;
  &:not(:last-child) {
    margin-bottom: 8px;
  }
`

const Calculate = props => {
  const { t } = props

  return (
    <DetailLayout>
      <DocumentTitle>{t('calculate_title')}</DocumentTitle>

      <Breadcrumb>
        <BreadcrumbItem href={ROUTES.ROOT_PATH}>{t('nav_home')}</BreadcrumbItem>
        <BreadcrumbItem>{t('calculate_title')}</BreadcrumbItem>
      </Breadcrumb>

      <Title>{t('calculate_title')}</Title>

      <Block>
        <SubTitle>Список услуг</SubTitle>
        {CONST.calculateService.list.map(item => {
          const id = prop('id', item)
          const title = prop('title', item)
          const active = prop('active', item)
          const href = ROUTES.CALCULATE_ITEM_PATH
          const asHref = prop('asHref', item)

          if (active) {
            return (
              <StyledLink key={id} href={href} asHref={asHref}>
                {t(title)}
              </StyledLink>
            )
          }

          return (
            <DisabledText key={id}>
              {t(title)}
            </DisabledText>
          )
        })}
      </Block>
    </DetailLayout>
  )
}

Calculate.propTypes = {
  t: PropTypes.func.isRequired
}

export default withTranslation()(Calculate)

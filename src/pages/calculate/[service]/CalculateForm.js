import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { equals, head, includes, not, path, pipe, prop, toPairs } from 'ramda'
import styled from 'styled-components'
import { useForm } from 'react-final-form'
import * as CONST from 'constants/backend'
import { devices } from 'constants/mediaQueries'
import { fieldNormalizeParams } from 'helpers/toNumber'
import { FieldWrap, FormWrapper } from 'components/StyledElements'
import { Field, UniversalStaticSelectField, AutoSave } from 'components/FormComponents'
import { InputField } from 'components/FormComponents/Basic'
import { ButtonPrimary } from 'components/Button'

const FlexFieldWrap = styled(FieldWrap)`
  @media (min-width: ${devices.tabletL}) {
    align-items: ${props => props.alignItems};
    display: flex;
    & > div {
      flex-grow: 1;
      width: calc(50% - 15px);
        &:first-child:not(:last-child) {
          margin-right: 10px;
        }
        &:last-child:not(:first-child) {
          margin-left: 10px;
        }
      }
      
    & ${FieldWrap} {
      margin-bottom: 0;
    }
  }
`

const fieldNames = {
  type: 'type',
  calculationType: 'calculation_type',
  number: 'number',
  numberInspection: 'number_inspection',
  numP: 'numP',
  numR: 'numR',
}

const getDisplayStyle = condition => ({
  display: condition ? 'block' : 'none'
})

const CalculateForm = props => {
  const { t, values, loading } = props

  const form = useForm()

  const type = path([fieldNames.type, 'id'], values)
  const calculationType = path([fieldNames.calculationType, 'id'], values)
  const numberInspection = prop(fieldNames.numberInspection, values)
  const calcTypeClearingValues = ['inspection_control', 'actualization']

  const showCalcType = not(includes(type, calcTypeClearingValues))
  const showNumberInspection = equals(type, 'inspection_control')
  const showNumRNumP = equals(calculationType, 'site')

  const onFormChange = value => {
    const [fieldName, fieldValue] = pipe(toPairs, head)(value)
    const fieldValueId = prop('id', fieldValue)

    if (equals(fieldName, fieldNames.type) && includes(fieldValueId, calcTypeClearingValues)) {
      if (calculationType) {
        form.change(fieldNames.calculationType, '')
      }
      if (numberInspection) {
        form.change(fieldNames.numberInspection, '')
      }
    }
  }

  return (
    <Fragment>
      <AutoSave
        debounce={200}
        onFormChange={onFormChange}
      />

      <FormWrapper>
        <FlexFieldWrap>
          <FieldWrap>
            <Field
              name={fieldNames.type}
              component={UniversalStaticSelectField}
              label={t('calculate_rate_type')}
              list={CONST.calculateRateType.list}
            />
          </FieldWrap>
          <FieldWrap style={getDisplayStyle(showCalcType)}>
            <Field
              name={fieldNames.calculationType}
              component={UniversalStaticSelectField}
              label={t('calculate_service_type')}
              list={CONST.calculateServiceType.list}
            />
          </FieldWrap>
        </FlexFieldWrap>

        <FlexFieldWrap>
          <FieldWrap>
            <Field
              name={fieldNames.number}
              component={InputField}
              label={'Количество нормативных документов на методы испытаний согласно заявляемой области аккредитации и'}
              type={'number'}
              {...fieldNormalizeParams}
            />
          </FieldWrap>

          <FieldWrap style={getDisplayStyle(showNumberInspection)}>
            <Field
              name={fieldNames.numberInspection}
              component={InputField}
              label={'количество проведенных испытаний за отчетный период'}
              type={'number'}
              {...fieldNormalizeParams}
            />
          </FieldWrap>
        </FlexFieldWrap>

        <FlexFieldWrap>
          {/*<FieldWrap style={getDisplayStyle(showNumRNumP)}>*/}
          {/*  <Field*/}
          {/*    name={fieldNames.numR}*/}
          {/*    component={InputField}*/}
          {/*    label={'количество несоответствий, выявленных при экспертизе документов'}*/}
          {/*    type={'number'}*/}
          {/*    {...fieldNormalizeParams}*/}
          {/*  />*/}
          {/*</FieldWrap >*/}
          <FieldWrap style={getDisplayStyle(showNumRNumP)}>
            <Field
              name={fieldNames.numP}
              component={InputField}
              label={'Количество персонала в заявляемой области аккредитации'}
              type={'number'}
              {...fieldNormalizeParams}
            />
          </FieldWrap>
        </FlexFieldWrap>

        <ButtonPrimary loading={loading} fullWidthMobile={true}>
          {t('calculate_submit')}
        </ButtonPrimary>
      </FormWrapper>
    </Fragment>
  )
}

CalculateForm.propTypes = {
  t: PropTypes.func.isRequired,
  values: PropTypes.object,
  loading: PropTypes.bool,
}

export default CalculateForm

import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import * as ROUTES from 'constants/routes'
import * as CONST from 'constants/backend'
import numberFormat from 'helpers/numberFormat'
import { withTranslation } from 'hocs/withTranslation'
import DocumentTitle from 'components/DocumentTitle'
import { DetailLayout } from 'components/Layouts'
import { Breadcrumb, BreadcrumbItem } from 'components/Breadcrumb'
import Title from 'components/Title'
import { Form } from 'components/FormComponents'
import CalculateFormTwo from './CalculateFormOssm'
import { path, prop } from 'ramda'

const Result = styled('div')`
  margin-top: 30px;
`

const ResultTitle = styled('div')`
  font-size: 18px;
  font-weight: 500;
  margin-bottom: 12px;
`

const ResultText = styled('div')`
  &:not(:last-child) {
    margin-bottom: 8px;
  }
`

const TotalCost = styled('span')`
  color: ${props => props.theme.colors.grey4};
`

const CalculateTestLab = props => {
  const { t, service, loading, result, onSubmit } = props

  const serviceTitle = CONST.calculateService.object[service]

  const totalSum = path(['data', 'results', 'sum'], result)
  const responseValues = prop('values', result)
  const type = prop('type', responseValues)
  const showResultExpense = type === 'inspection_control'

  return (
    <DetailLayout>
      <DocumentTitle>{t('calculate_title')}</DocumentTitle>

      <Breadcrumb>
        <BreadcrumbItem href={ROUTES.ROOT_PATH}>{t('nav_home')}</BreadcrumbItem>
        <BreadcrumbItem href={ROUTES.CALCULATE_PATH}>{t('calculate_title')}</BreadcrumbItem>
        <BreadcrumbItem>{t(serviceTitle)}</BreadcrumbItem>
      </Breadcrumb>

      <Title>{t('calculate_title')}</Title>

      <Form onSubmit={onSubmit}>
        <CalculateFormTwo t={t} loading={loading} />
      </Form>

      {result && (
        <Result>
          <ResultTitle>{t('calculate_result_title')}</ResultTitle>

          <ResultText>
            {t('calculate_result_cost')}
            <TotalCost>{numberFormat(totalSum, 'UZS')}</TotalCost>
          </ResultText>
          <ResultText>{t('calculate_result_nds')}</ResultText>
          {showResultExpense && (
            <ResultText>{t('calculate_result_expense')}</ResultText>
          )}
        </Result>
      )}
    </DetailLayout>
  )
}

CalculateTestLab.propTypes = {
  t: PropTypes.func.isRequired,
  service: PropTypes.string,
  result: PropTypes.object,
  loading: PropTypes.bool.isRequired,
  onSubmit: PropTypes.func.isRequired,
}

export default withTranslation()(CalculateTestLab)

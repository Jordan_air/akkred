import React, { useState } from 'react'
import { not, path } from 'ramda'
import { useToasts } from 'react-toast-notifications'
import * as API from 'constants/api'
import { mapResponseToFormError } from 'helpers/form'
import CalculateTestLabTwo from './CalculateTestLabOk'

const serializer = values => ({
  ...values,
  type: path(['type', 'id'], values),
  calculation_type: path(['calculation_type', 'id'], values)
})

const CalculateTestLabOkContainer = props => {
  const [loading, setLoading] = useState(false)
  const [result, setResult] = useState(null)
  const { addToast } = useToasts()

  async function onSubmit (values) {
    setLoading(true)

    const serializedValues = serializer(values)
    const body = JSON.stringify(serializedValues)
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      },
      body
    }
    const data = await fetch(API.CALCULATE_CREATE_OK, options)
      .then(response => {
        setLoading(false)

        if (not(response.ok)) {
          return Promise.reject(response.json())
        }

        return response.json()
      })
      .then(response => {
        setResult({
          data: response,
          values: serializedValues
        })
      })
      .catch(response => {
        addToast('Ошибка', { appearance: 'error' })
        setResult(null)
        return response.then(mapResponseToFormError)
      })

    return data
  }

  return (
    <CalculateTestLabTwo
      loading={loading}
      result={result}
      onSubmit={onSubmit}
      {...props}
    />
  )
}

export default CalculateTestLabOkContainer

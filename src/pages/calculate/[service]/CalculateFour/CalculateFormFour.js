import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { equals, head, includes, not, path, pipe, prop, toPairs } from 'ramda'
import styled from 'styled-components'
import { useForm } from 'react-final-form'
import * as CONST from 'constants/backend'
import { devices } from 'constants/mediaQueries'
import { fieldNormalizeParams } from 'helpers/toNumber'
import { FieldWrap, FormWrapper } from 'components/StyledElements'
import { Field, UniversalStaticSelectField, AutoSave } from 'components/FormComponents'
import { InputField } from 'components/FormComponents/Basic'
import { ButtonPrimary } from 'components/Button'

const FlexFieldWrap = styled(FieldWrap)`
  @media (min-width: ${devices.tabletL}) {
    align-items: ${props => props.alignItems};
    display: flex;
    & > div {
      flex-grow: 1;
      width: calc(50% - 15px);
        &:first-child:not(:last-child) {
          margin-right: 10px;
        }
        &:last-child:not(:first-child) {
          margin-left: 10px;
        }
      }
      
    & ${FieldWrap} {
      margin-bottom: 0;
    }
  }
`

const fieldNames = {
  type: 'type',
  calculationType: 'calculation_type',
  numND: 'numND',
  num_test: 'num_test',
  numStaff: 'numStaff',
  numObj: 'numObj',
}

const getDisplayStyle = condition => ({
  display: condition ? 'block' : 'none'
})

const CalculateForm = props => {
  const { t, values, loading } = props

  const form = useForm()

  const type = path([fieldNames.type, 'id'], values)
  const calculationType = path([fieldNames.calculationType, 'id'], values)
  const numberInspection = prop(fieldNames.numberInspection, values)
  const calcTypeClearingValues = ['inspection_control', 'actualization']
  const NumTestClearingValues = ['inspection_control']
  const NumNdClearingValues = ['inspection_control']
  const NumObjClearingValues = ['expertise']
  const NumStaffClearingValues = ['expertise']

  const showCalcType = not(includes(type, calcTypeClearingValues))
  const showNumTest = includes(type, NumTestClearingValues)
  const showNumNd = not(includes(type, NumNdClearingValues))
  const showNumObj = includes(calculationType, NumObjClearingValues)
  const showNumStaff = not(includes(calculationType, NumStaffClearingValues))

  const onFormChange = value => {
    const [fieldName, fieldValue] = pipe(toPairs, head)(value)
    const fieldValueId = prop('id', fieldValue)

    if (equals(fieldName, fieldNames.type) && includes(fieldValueId, calcTypeClearingValues)) {
      if (calculationType) {
        form.change(fieldNames.calculationType, '')
      }
      if (numberInspection) {
        form.change(fieldNames.numberInspection, '')
      }
    }
  }

  return (
    <Fragment>
      <AutoSave
        debounce={200}
        onFormChange={onFormChange}
      />

      <FormWrapper>
        <FlexFieldWrap>
          <FieldWrap>
            <Field
              name={fieldNames.type}
              component={UniversalStaticSelectField}
              label={t('calculate_rate_type')}
              list={CONST.calculateRateType.list}
            />
          </FieldWrap>
          <FieldWrap style={getDisplayStyle(showCalcType)}>
            <Field
              name={fieldNames.calculationType}
              component={UniversalStaticSelectField}
              label={t('calculate_service_type')}
              list={CONST.calculateServiceType.list}
            />
          </FieldWrap>
        </FlexFieldWrap>

        <FlexFieldWrap>
          <FieldWrap style={getDisplayStyle(showNumNd)}>
            <Field
              name={fieldNames.numND}
              component={InputField}
              label={'Количество нормативных документов на объекты сертификации согласно проекту Области аккредитации.'}
              type={'number'}
              {...fieldNormalizeParams}
            />
          </FieldWrap>
        </FlexFieldWrap>

        <FlexFieldWrap style={getDisplayStyle(showNumTest)}>
          <FieldWrap>
            <Field
              name={fieldNames.num_test}
              component={InputField}
              label={'Количество комплектов документов работ по подтверждению соответствия за отчетный период'}
              type={'number'}
              {...fieldNormalizeParams}
            />
          </FieldWrap>
        </FlexFieldWrap>

        <FlexFieldWrap>
          <FieldWrap style={getDisplayStyle(showNumObj)}>
            <Field
              name={fieldNames.numObj}
              component={InputField}
              label={t('calculate_numObj')}
              type={'number'}
              {...fieldNormalizeParams}
            />
          </FieldWrap>
          <FieldWrap style={getDisplayStyle(showNumStaff)}>
            <Field
              name={fieldNames.numStaff}
              component={InputField}
              label={t('calculate_numStaff')}

              type={'number'}
              {...fieldNormalizeParams}
            />
          </FieldWrap>

        </FlexFieldWrap>

        <ButtonPrimary loading={loading} fullWidthMobile={true}>
          {t('calculate_submit')}
        </ButtonPrimary>
      </FormWrapper>
    </Fragment>
  )
}

CalculateForm.propTypes = {
  t: PropTypes.func.isRequired,
  values: PropTypes.object,
  loading: PropTypes.bool,
}

export default CalculateForm

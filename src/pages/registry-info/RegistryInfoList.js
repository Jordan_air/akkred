import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { prop, path } from 'ramda'
import * as CONST from 'constants/backend'
import styled from 'styled-components'
import dateFormat from 'helpers/dateFormat'
import {
  Table,
  TableHeader,
  TableBody,
  TableRow,
  TableCol
} from 'components/Table'

const ListCounts = styled('div')`
  font-size: 18px;
  margin-top: 25px;
`
const Count = styled('span')`
  color: ${props => props.theme.colors.grey4};
  font-weight: 500;
`
const TableContainer = styled('div')`
  text-align: start;
`

const RegistryInfoList = props => {
  const { t, data, count} = props

  return (
    <Fragment>
      <TableContainer>
        <Table>
          <TableHeader>
            <TableRow>
              <TableCol span={10}>{t('registry_table_legal')}</TableCol>
              <TableCol span={6}>{t('registry_table_number')}</TableCol>
              <TableCol span={4}>{t('registry_table_date')}</TableCol>
              <TableCol span={4} align={'right'}>{t('registry_info_status')}</TableCol>
            </TableRow>
          </TableHeader>
          <TableBody>
            {data.map(item => {
              const id = prop('id', item)
              const registryName = path(['reestr', 'title_organ'], item)
              const registryNumber = path(['reestr', 'number'], item)
              const date = dateFormat(prop('date', item))
              const status = prop('status', item)
              const statusName = CONST.registryInfoStatus.object[status]

              return (
                <TableRow key={id}>
                  <TableCol span={10}>{registryName}</TableCol>
                  <TableCol span={6}>{registryNumber}</TableCol>
                  <TableCol span={4}>{date}</TableCol>
                  <TableCol span={4} align={'right'}>{t(statusName)}</TableCol>
                </TableRow>
              )
            })}
          </TableBody>
        </Table>
      </TableContainer>
      <ListCounts>
        {t('registry_query_count')}: <Count>{count}</Count>
      </ListCounts>
    </Fragment>
  )
}

RegistryInfoList.propTypes = {
  t: PropTypes.func.isRequired,
  data: PropTypes.array.isRequired,
}

RegistryInfoList.defaultProps = {
  data: [],
  count: 0,
}

export default RegistryInfoList

import React from 'react'
import PropTypes from 'prop-types'
import { prop } from 'ramda'
import * as ROUTES from 'constants/routes'
import { withTranslation } from 'hocs/withTranslation'
import DocumentTitle from 'components/DocumentTitle'
import { BaseLayout } from 'components/Layouts'
import { Breadcrumb, BreadcrumbItem } from 'components/Breadcrumb'
import { TitleBig } from 'components/Title'
import { Row, Col } from 'components/Grid'
import Pagination from 'components/Pagination'
import RegistryInfoList from './RegistryInfoList'
import RegistryInfoFilter from './RegistryInfoFilter'

const RegistryInfo = props => {
  const { t, data, onFormReset, onFormChange, initialValues } = props

  const list = prop('results', data)
  const count = prop('count', data)

  return (
    <BaseLayout>
      <DocumentTitle>{t('registry_info_title')}</DocumentTitle>

      <Breadcrumb>
        <BreadcrumbItem href={ROUTES.ROOT_PATH}>{t('nav_home')}</BreadcrumbItem>
        <BreadcrumbItem>{t('registry_info_title')}</BreadcrumbItem>
      </Breadcrumb>

      <TitleBig>{t('registry_info_title')}</TitleBig>

      <Row>
        <Col span={8}>
          <RegistryInfoFilter
            t={t}
            onFormReset={onFormReset}
            onFormChange={onFormChange}
            initialValues={initialValues}
          />
        </Col>
        <Col span={16}>
          <RegistryInfoList t={t} data={list} count={count}/>
          <Pagination totalRecords={count} />
        </Col>
      </Row>
    </BaseLayout>
  )
}

RegistryInfo.propTypes = {
  t: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
  onFormReset: PropTypes.func.isRequired,
  onFormChange: PropTypes.func.isRequired,
  initialValues: PropTypes.object.isRequired
}

export default withTranslation()(RegistryInfo)

import React from 'react'
import * as API from 'constants/api'
import fetchData from 'helpers/fetchData'
import Financial from './Financial'

const FinancialContainer = props => (
  <Financial {...props} />
)

FinancialContainer.getInitialProps = async () => {
  const params = { pageSize: 1000 }
  const data = await fetchData(API.FINANCIAL_LIST, params)

  return {
    data,
    namespacesRequired: ['common']
  }
}

export default FinancialContainer

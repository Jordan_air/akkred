import React from 'react'
import App from 'next/app'
import { prop } from 'ramda'
import { ThemeProvider } from 'styled-components'
import { ToastProvider } from 'react-toast-notifications'
import theme from 'constants/theme'
import { MENU_LIST, NEWS_LIST } from 'constants/api'
import fetchData from 'helpers/fetchData'
import scrollToTop from 'helpers/scrollToTop'
import { appWithTranslation } from 'hocs/withTranslation'
// import { Router } from 'components/I18N'
import { ToastContainer, Toast } from 'components/Toast'
import NormalizedStyles from 'components/NormalizedStyles'
import GlobalStyles from 'components/GlobalStyles'
import TranslationWrapper from 'components/TranslationWrapper'
import AppContext from 'components/Contexts/AppContext'
import 'bootstrap/dist/css/bootstrap.css'

const STORAGE_MENU_KEY = 'menu'
class MyApp extends App {
  constructor(props) {
    super(props)
    this.state = {
      pageLoading: false,
      menu: [],
      news: [],
    }
    this.showLoading = this.showLoading.bind(this)
    this.hideLoading = this.hideLoading.bind(this)
    this.onRouteChangeStart = this.onRouteChangeStart.bind(this)
    this.onRouteChangeComplete = this.onRouteChangeComplete.bind(this)
  }

  showLoading() {
    this.setState({ pageLoading: true })
  }

  hideLoading() {
    this.setState({ pageLoading: false })
  }

  onRouteChangeStart() {
    this.showLoading()
  }

  onRouteChangeComplete() {
    if (typeof window === 'object') {
      scrollToTop()
    }
    this.hideLoading()
  }

  componentDidMount() {
    // Fetch navigation menu
    const storageMenu = sessionStorage.getItem(STORAGE_MENU_KEY)
    if (storageMenu) {
      this.setState({
        menu: JSON.parse(storageMenu),
      })
    } else {
      fetchData(MENU_LIST).then(({ data }) => {
        this.setState({ menu: data })
        sessionStorage.setItem(STORAGE_MENU_KEY, JSON.stringify(data))
      })
    }

    // Fetch side news
    fetchData(NEWS_LIST, { pageSize: 4 }).then((response) => {
      const results = prop('results', response)
      this.setState({ news: results })
    })

    // Router.events.on('routeChangeStart', this.onRouteChangeStart)
    // Router.events.on('routeChangeComplete', this.onRouteChangeComplete)
    // Router.events.on('routeChangeError', this.hideLoading)
  }

  render() {
    const { Component, pageProps } = this.props
    const { pageLoading, menu, news } = this.state

    const toastParams = {
      autoDismiss: true,
      components: {
        Toast,
        ToastContainer,
      },
      placement: 'top-left',
    }

    const appContextValue = { menu, news }

    return (
      <ThemeProvider theme={theme}>
        <ToastProvider {...toastParams}>
          {pageLoading ? 'Loading...' : null}
          <NormalizedStyles />
          <GlobalStyles />

          <AppContext.Provider value={appContextValue}>
            <TranslationWrapper>
              <Component {...pageProps} />
            </TranslationWrapper>
          </AppContext.Provider>
        </ToastProvider>
      </ThemeProvider>
    )
  }
}

export default appWithTranslation(MyApp)

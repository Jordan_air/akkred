import React from 'react'
import PropTypes from 'prop-types'
import { defaultTo, groupBy, head, path, pipe, prop, toPairs } from 'ramda'
import * as ROUTES from 'constants/routes'
import getTranslate from 'helpers/getTranslate'
import { withTranslation } from 'hocs/withTranslation'
import DocumentTitle from 'components/DocumentTitle'
import { DetailLayout } from 'components/Layouts'
import { Breadcrumb, BreadcrumbItem } from 'components/Breadcrumb'
import Title from 'components/Title'
import {
  Table,
  TableHeader,
  TableBody,
  TableRow,
  TableCol,
  TableGroup
} from 'components/Table'

const Inspection = props => {
  const { t } = props

  const newList = [
    {
      id:'2022',
      name:'2022'
    }
  ]

  return (
    <DetailLayout>
      <DocumentTitle>{t('inspection_title')}</DocumentTitle>

      <Breadcrumb>
        <BreadcrumbItem href={ROUTES.ROOT_PATH}>{t('nav_home')}</BreadcrumbItem>
        <BreadcrumbItem>{t('inspection_title')}</BreadcrumbItem>
      </Breadcrumb>

      <Title>{t('inspection_title')}</Title>

      <Table>
        <TableHeader>
          <TableRow>
            <TableCol span={2}>№</TableCol>
            <TableCol span={16}>{t('documents_name')}</TableCol>
            <TableCol span={6} />
          </TableRow>
        </TableHeader>

        {newList.map(cat => {
          const id = prop('id', cat)
          const name = prop('name', cat)

          return (
            <TableBody id={id}>
              <TableGroup><a href={`https://akkred.uz/inspection?year=${id}`}> {name}</a></TableGroup>
            </TableBody>
          )
        })}
      </Table>
    </DetailLayout>
  )
}



export default withTranslation()(Inspection)

import React from 'react'
import * as API from 'constants/api'
import fetchData from 'helpers/fetchData'
import Home from './Home'

const HomeContainer = props => (
  <Home {...props} />
)

HomeContainer.getInitialProps = async () => {
  const sliderData = await fetchData(API.SLIDER_LIST)
  const agenciesData = await fetchData(API.TYPE_ORGAN_STATIC_LIST)

  return {
    sliderData,
    agenciesData,
    namespacesRequired: ['common']
  }
}

export default HomeContainer

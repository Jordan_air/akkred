import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { prop } from 'ramda'
import styled from 'styled-components'
import { withTranslation } from 'hocs/withTranslation'
import DocumentTitle from 'components/DocumentTitle'
import { BaseLayout } from 'components/Layouts'
import Slider from 'components/Slider'
import Agencies from './components/Agencies'
import Stats from './components/Stats'
import NewsBlock from './components/NewsBlock'
import Services from './components/Services'

const HomeLayout = styled(BaseLayout)`
  background-color: ${props => props.theme.colors.grey};
`

const Home = props => {
  const { t, sliderData, agenciesData } = props

  const sliderList = prop('results', sliderData)
  const agenciesList = prop('results', agenciesData)

  return (
    <Fragment>
      <DocumentTitle>{t('header_title')}</DocumentTitle>

      <HomeLayout>
        <Slider data={sliderList} />

        <Agencies data={agenciesList} />

        <Stats />

        <NewsBlock />

        <Services />
      </HomeLayout>
    </Fragment>
  )
}

Home.propTypes = {
  t: PropTypes.func.isRequired,
  sliderData: PropTypes.object.isRequired,
  agenciesData: PropTypes.object.isRequired
}

export default withTranslation()(Home)

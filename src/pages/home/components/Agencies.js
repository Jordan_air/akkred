import React from 'react'
import PropTypes from 'prop-types'
import { prop } from 'ramda'
import styled from 'styled-components'
import { useRouter } from 'next/router'
import { ArrowRight } from 'react-feather'
import * as ROUTES from 'constants/routes'
import { devices } from 'constants/mediaQueries'
import getTranslate from 'helpers/getTranslate'
import hexToRgb from 'helpers/hexToRgb'
import { withTranslation } from 'hocs/withTranslation'

const Wrapper = styled('div')`
  display: flex;
  flex-wrap: wrap;
  margin: 50px 0;
  @media (min-width: ${devices.tabletL}) {
    flex-wrap: unset;
  }
`

const Agency = styled('div')`
  background-color: white;
  border-bottom: ${props => props.theme.borderLight};
  cursor: pointer;
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  justify-content: space-between;
  min-width: 100%;
  padding: 25px;
  position: relative;
  &:last-child {
    border: none;
  }
  &:after {
    background-color: inherit;
    content: "";
    height: 12px;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    transition: ${props => props.theme.transition};
  }
  &:hover {
    color: ${props => props.theme.colors.primary};
    &:after {
      transform: translateY(-12px);
    }
  }
  
  @media (min-width: ${devices.tabletL}) {
    border-right: ${props => props.theme.borderLight};
    border-bottom: none;
    min-width: 20%;
  }
`

const Title = styled('div')`
  line-height: 22px;
  margin-bottom: 6px;
  transition: ${props => props.theme.transition};
  ${Agency}:hover & {
    transform: translateY(-12px);
  }
`

const IconButton = styled('div')`
  align-items: center;
  color: ${hexToRgb('#000000', '0.35')};
  font-size: 13px;
  display: flex;
  transition: ${props => props.theme.transition};
  & > svg {
    margin-left: 10px;
    opacity: 0;
    transition: ${props => props.theme.transition};
    transform: translateX(-5px);
  }
  ${Agency}:hover & {
    color: ${props => props.theme.colors.primary};
    & > svg {
      color: ${props => props.theme.colors.primary};
      opacity: 1;
      transform: translateX(0);
    }
  }
`

const Agencies = props => {
  const { t, data } = props

  const router = useRouter()

  const onClick = id => () => {
    return router.push({
      pathname: ROUTES.REGISTRY_PATH,
      query: {
        status: 'active-extended',
        typeOrgan: id
      }
    })
  }

  return (
    <Wrapper>
      {data.map(item => {
        const id = prop('id', item)
        const count = prop('count', item)
        const title = getTranslate(item)

        return (
          <Agency key={id} onClick={onClick(id)}>
            <Title>{title} ({count})</Title>
            <IconButton>
              {t('more')}
              <ArrowRight size={18} />
            </IconButton>
          </Agency>
        )
      })}
    </Wrapper>
  )
}

Agencies.propTypes = {
  t: PropTypes.func.isRequired,
  data: PropTypes.array.isRequired,
}

export default withTranslation()(Agencies)

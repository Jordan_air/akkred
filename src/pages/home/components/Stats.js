import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { devices } from 'constants/mediaQueries'
import { withTranslation } from 'hocs/withTranslation'
import Books from '../icons/Books'
import Calendar from '../icons/Calendar'
import Hand from '../icons/Hand'
import Seal from '../icons/Seal'
import { prop } from 'ramda'

const Wrapper = styled('div')`
  display: grid;
  grid-gap: 40px;
  margin: 50px 20px;
  @media (min-width: ${devices.tabletL}) {
    grid-template-columns: repeat(2, 1fr);
    margin: 50px 60px;
  }
`

const Stat = styled('div')`
  align-items: center;
  display: flex;
`

const IconWrapper = styled('div')`
  align-items: center;
  background-color: ${props => props.theme.colors.primary};
  border-radius: 20px;
  display: flex;
  justify-content: center;
  height: 65px;
  margin-right: 15px;
  padding: 10px;
  width: 65px;
  & > svg {
    color: white;
    font-size: 45px;
  }
`

const StatInfo = styled('div')`
  flex-grow: 1;
`

const Count = styled('div')`
  font-size: 24px;
  font-weight: 500;
`

const Text = styled('div')`
  margin-top: 5px;
`

const data = [
  {
    count: 98,
    text: 'home_stats_1',
    icon: <Calendar />
  },
  {
    count: 107,
    text: 'home_stats_2',
    icon: <Books />
  },
  {
    count: 116,
    text: 'home_stats_3',
    icon: <Seal />
  },
  {
    count: 59,
    text: 'home_stats_4',
    icon: <Hand />
  },
]

const Stats = props => {
  const { t } = props

  return (
    <Wrapper>
      {data.map((item, index) => {
        const count = prop('count', item)
        const text = prop('text', item)
        const icon = prop('icon', item)

        return (
          <Stat key={index}>
            <IconWrapper>
              {icon}
            </IconWrapper>

            <StatInfo>
              <Count>{count}</Count>
              <Text>{t(text)}</Text>
            </StatInfo>
          </Stat>
        )
      })}
    </Wrapper>
  )
}

Stats.propTypes = {
  t: PropTypes.func.isRequired
}

export default withTranslation()(Stats)

import React from 'react'
import PropTypes from 'prop-types'
import { defaultTo, groupBy, head, path, pipe, prop, toPairs } from 'ramda'
import styled from 'styled-components'
import * as ROUTES from 'constants/routes'
import getTranslate from 'helpers/getTranslate'
import { withTranslation } from 'hocs/withTranslation'
import DocumentTitle from 'components/DocumentTitle'
import { DetailLayout } from 'components/Layouts'
import { Breadcrumb, BreadcrumbItem } from 'components/Breadcrumb'
import Title from 'components/Title'
import {
  Table,
  TableHeader,
  TableBody,
  TableRow,
  TableGroup,
  TableCol
} from 'components/Table'

const ExtLink = styled('a')`
  color: ${props => props.theme.colors.primary};
  text-decoration: none;
`

const DocumentsDetail = props => {
  const { t, data } = props

  const documentName = getTranslate(data)

  // const formList = pipe(
  //   prop('document_forms'),
  //   defaultTo([]),
  //   groupBy(path(['category', 'id'])),
  //   toPairs
  // )(data)

  const forms = prop('forms', data)

  return (
    <DetailLayout>
      <DocumentTitle>{documentName}</DocumentTitle>

      <Breadcrumb>
        <BreadcrumbItem href={ROUTES.ROOT_PATH}>{t('nav_home')}</BreadcrumbItem>
        <BreadcrumbItem href={ROUTES.DOCUMENTS_PATH}>{t('documents_title')}</BreadcrumbItem>
        <BreadcrumbItem>{documentName}</BreadcrumbItem>
      </Breadcrumb>

      <Title>{documentName}</Title>

      <Table>
        <TableHeader>
          <TableRow>
            <TableCol span={2}>№</TableCol>
            <TableCol span={16}>{t('documents_name')}</TableCol>
            <TableCol span={6} align={'right'}>{t('link')}</TableCol>
          </TableRow>
        </
          TableHeader>

        {forms.map(item => {
          const childrens = prop('childrens', item)
          const category = prop('category', item)
          const id = prop('id', category)
          const title = getTranslate(category, 'title')

          return (
            <TableBody key={id} >
              <TableGroup>{title}</TableGroup>
              {childrens.map((doc, index) => {
                const id = prop('id', doc)
                const order = index + 1
                const fileRu = prop('file_ru', doc)
                const title = getTranslate(doc, 'title')
                const file = getTranslate(doc, 'file')

                return (
                  <TableRow key={id} isBody={true}>
                    <TableCol span={2}>{order}</TableCol>
                    <TableCol span={16}>{title}</TableCol>
                    <TableCol span={6} align={'right'}>
                      {
                        file ? (
                          <ExtLink href={`https://akkred.uz:8081/media/${file}`}
                            rel={'noopener noreferrer'} target={'_blank'}>
                            {t('download')}
                          </ExtLink>
                        )
                          : (
                            <ExtLink href={`https://akkred.uz:8081/media/${fileRu}`}
                              rel={'noopener noreferrer'} target={'_blank'}>
                              {t('download')}
                            </ExtLink>
                          )
                      }
                    </TableCol>
                  </TableRow>
                )
              })}
            </TableBody>
          )
        })}

        {/* {formList.map(item => { */}
        {/*  const [category, list] = item */}
        {/*  const categoryName = pipe(head, path(['category', 'title']))(list) */}

        {/*  return ( */}
        {/*    <TableBody key={category}> */}
        {/*      <TableGroup>{categoryName}</TableGroup> */}
        {/*      {list.map((doc, index) => { */}
        {/*        const id = prop('id', doc) */}
        {/*        const order = index + 1 */}
        {/*        const title = prop('title', doc) */}
        {/*        const file = prop('file', doc) */}

        {/*        return ( */}
        {/*          <TableRow key={id} isBody={true}> */}
        {/*            <TableCol span={2}>{order}</TableCol> */}
        {/*            <TableCol span={16}>{title}</TableCol> */}
        {/*            <TableCol span={6} align={'right'}> */}
        {/*              {file && ( */}
        {/*                <ExtLink href={file} rel={'noopener noreferrer'} target={'_blank'}> */}
        {/*                  {t('download')} */}
        {/*                </ExtLink> */}
        {/*              )} */}
        {/*            </TableCol> */}
        {/*          </TableRow> */}
        {/*        ) */}
        {/*      })} */}
        {/*    </TableBody> */}
        {/*  ) */}
        {/* })} */}
      </Table>
    </DetailLayout>
  )
}

DocumentsDetail.propTypes = {
  t: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired
}

export default withTranslation()(DocumentsDetail)

import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { prop, path, groupBy, pipe, toPairs, head } from 'ramda'
import { sprintf } from 'sprintf-js'
import styled from 'styled-components'
import { useRouter } from 'next/router'
import * as ROUTES from 'constants/routes'
import getTranslate from 'helpers/getTranslate'
import { withTranslation } from 'hocs/withTranslation'
import DocumentTitle from 'components/DocumentTitle'
import { DetailLayout } from 'components/Layouts'
import { Breadcrumb, BreadcrumbItem } from 'components/Breadcrumb'
import Title from 'components/Title'
import {
  Table,
  TableHeader,
  TableBody,
  TableRow,
  TableGroup,
  TableCol
} from 'components/Table'
import Link from 'components/Link'

const ExtLink = styled('a')`
  color: ${props => props.theme.colors.primary};
  text-decoration: none;
`
const ClickableRow = styled(TableGroup)`
  cursor: pointer;
`

const StyledLink = styled(Link)`
  font-weight: normal;
`

const Documents = props => {
  const { t, docData } = props

  const { query } = useRouter()
  const parents = prop('parents', query)
  const [activeRegistry, setActiveRegistry] = useState(false)

  // const onOpenRegistry = () => {
  //   if (activeRegistry === true) {
  //     setActiveRegistry(false)
  //   } else {
  //     setActiveRegistry(true)
  //   }
  // }

  const onOpenRegistry = id => () => {
    setActiveRegistry(id)
  }

  const onCloseRegistry = () => {
    setActiveRegistry(null)
  }

  const results = prop('results', docData)
  const list = pipe(
    groupBy(path(['type', 'id'])),
    toPairs
  )(results)

  const title = parents
    ? pipe(head, prop('parents'), getTranslate)(results)
    : t('documents_title')

  return (
    <DetailLayout>
      <DocumentTitle>{title}</DocumentTitle>

      <Breadcrumb>
        <BreadcrumbItem href={ROUTES.ROOT_PATH}>{t('nav_home')}</BreadcrumbItem>
        <BreadcrumbItem>{title}</BreadcrumbItem>
      </Breadcrumb>

      <Title>{title}</Title>

      <Table>
        <TableHeader>
          <TableRow>
            <TableCol span={2}>№</TableCol>
            <TableCol span={6}>{t('documents_desc')}</TableCol>
            <TableCol span={12}>{t('documents_name')}</TableCol>
            <TableCol span={4} align={'right'}>{t('link')}</TableCol>
          </TableRow>
        </TableHeader>

        {list.map(item => {
          const [docTypeId, docList] = item
          const docType = pipe(head, prop('type'))(docList)
          const docTypeName = getTranslate(docType)

          return (
            <TableBody key={docTypeId}>

              {activeRegistry ? (
                <div>
                  <ClickableRow onClick={onCloseRegistry}>{docTypeName}</ClickableRow>
                  {docList.map((doc, index) => {
                    const docId = prop('id', doc)
                    const status = prop('status', doc)
                    const order = index + 1
                    const file = prop('file', doc)
                    const link = prop('link', doc)
                    const docName = getTranslate(doc)
                    const description = getTranslate(doc, 'description')

                    const isFile = status === 'file'
                    const isLink = status === 'link'
                    const isForm = status === 'form'

                    return (
                      <TableRow key={docId} isBody={true}>
                        <TableCol span={2}>{order}</TableCol>
                        <TableCol span={6}>{description}</TableCol>
                        <TableCol span={12}>{docName}</TableCol>
                        <TableCol span={4} align={'right'}>
                          {isForm && (
                            <StyledLink
                              href={ROUTES.DOCUMENTS_ITEM_PATH}
                              asHref={sprintf(ROUTES.DOCUMENTS_ITEM_URL, docId)}>
                              {t('form_link')}
                            </StyledLink>
                          )}
                          {(file && isFile) && (
                            <ExtLink href={file} rel={'noopener noreferrer'} target={'_blank'}>
                              {t('download')}
                            </ExtLink>
                          )}
                          {(link && isLink) && (
                            <ExtLink href={link} rel={'noopener noreferrer'} target={'_blank'}>
                              {t('link')}
                            </ExtLink>
                          )}
                        </TableCol>
                      </TableRow>
                    )
                  })}

                </div>

              ) : (
                <ClickableRow onClick={onOpenRegistry(docTypeId)}>{docTypeName}</ClickableRow>
              )}

            </TableBody>
          )
        })}
      </Table>
    </DetailLayout>
  )
}

Documents.propTypes = {
  t: PropTypes.func.isRequired,
  docData: PropTypes.object.isRequired,
}

export default withTranslation()(Documents)

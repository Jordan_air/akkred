import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { map } from 'ramda'
import * as CONST from 'constants/backend'
import * as ROUTES from 'constants/routes'
import { withTranslation } from 'hocs/withTranslation'
import DocumentTitle from 'components/DocumentTitle'
import { DetailLayout } from 'components/Layouts'
import { Breadcrumb, BreadcrumbItem } from 'components/Breadcrumb'
import Title from 'components/Title'
import {
  Form,
  Field,
  RadioGroup,
  InputField,
  UniversalStaticSelectField,
} from 'components/FormComponents'
import { Radio } from 'components/Toggle'
import { FormWrapper, FieldWrap } from 'components/StyledElements'
import { ButtonPrimary } from 'components/Button'

const InlineRadioGroup = props => (
  <RadioGroup type={'inline'} {...props} />
)

const mapRadioButtons = (list, t) => map(item => (
  <Radio
    key={item.id}
    value={item.id}
    label={t(item.title)}
  />
), list)

const mapMarkRadioButtons = map(item => (
  <Radio
    key={item.id}
    value={item.id}
    label={item.title}
  />
), CONST.markType.list)

const SatisfactionRating = props => {
  const { t, onSubmit, loading } = props

  return (
    <DetailLayout>
      <DocumentTitle>{t('rating_title')}</DocumentTitle>

      <Breadcrumb>
        <BreadcrumbItem href={ROUTES.ROOT_PATH}>{t('nav_home')}</BreadcrumbItem>
        <BreadcrumbItem>{t('rating_title')}</BreadcrumbItem>
      </Breadcrumb>

      <Title>{t('rating_title')}</Title>

      <Form
        onSubmit={onSubmit}
        resetOnSuccess={true}
        keepDirtyOnReinitialize={false}>
        <Fragment>
          <FormWrapper>
            {/* 1 */}
            <FieldWrap>
              <Field
                name={'type_of_activity'}
                component={UniversalStaticSelectField}
                label={t('rating_label_1')}
                list={CONST.activityType.list}
              />
            </FieldWrap>

            {/* 2 */}
            <FieldWrap>
              <Field
                name={'working_with_ozakk'}
                component={InlineRadioGroup}
                label={t('rating_label_2')}>
                {mapRadioButtons(CONST.workingType.list, t)}
              </Field>
            </FieldWrap>

            {/* 3 */}
            <FieldWrap>
              <Field
                name={'mark_our_service'}
                component={InlineRadioGroup}
                label={t('rating_label_3')}>
                {mapMarkRadioButtons}
              </Field>
            </FieldWrap>

            {/* 4 */}
            <FieldWrap>
              <Field
                name={'source_information'}
                component={InlineRadioGroup}
                label={t('rating_label_4')}>
                {mapRadioButtons(CONST.sourceType.list, t)}
              </Field>
            </FieldWrap>

            {/* 5 */}
            <FieldWrap>
              <Field
                name={'mark_info_applicants'}
                component={InlineRadioGroup}
                label={t('rating_label_5')}>
                {mapMarkRadioButtons}
              </Field>
            </FieldWrap>

            {/* 6 */}
            <FieldWrap>
              <Field
                name={'mark_employee_attitude'}
                component={InlineRadioGroup}
                label={t('rating_label_6')}>
                {mapRadioButtons(CONST.employeeAttitudeType.list, t)}
              </Field>
            </FieldWrap>

            {/* 7 */}
            <FieldWrap>
              <Field
                name={'mark_deadline'}
                component={InlineRadioGroup}
                label={t('rating_label_7')}>
                {mapMarkRadioButtons}
              </Field>
            </FieldWrap>

            {/* 8 */}
            <FieldWrap>
              <Field
                name={'mark_competence_appraisers'}
                component={InlineRadioGroup}
                label={t('rating_label_8')}>
                {mapMarkRadioButtons}
              </Field>
            </FieldWrap>

            {/* 9 */}
            <FieldWrap>
              <Field
                name={'mark_objectivity_ozakk'}
                component={InlineRadioGroup}
                label={t('rating_label_9')}>
                {mapMarkRadioButtons}
              </Field>
            </FieldWrap>

            {/* 10 */}
            <FieldWrap>
              <Field
                name={'mark_confidentiality_ozakk'}
                component={InlineRadioGroup}
                label={t('rating_label_10')}>
                {mapMarkRadioButtons}
              </Field>
            </FieldWrap>

            {/* 11 */}
            <FieldWrap>
              <Field
                name={'trainings'}
                component={InputField}
                label={t('rating_label_11')}
              />
            </FieldWrap>

            {/* 12 */}
            <FieldWrap>
              <Field
                name={'site_info'}
                component={InputField}
                label={t('rating_label_12')}
              />
            </FieldWrap>

            {/* 13 */}
            <FieldWrap>
              <Field
                name={'offers'}
                component={InputField}
                label={t('rating_label_13')}
              />
            </FieldWrap>

            {/* 14 */}
            <FieldWrap>
              <Field
                name={'questionnaire'}
                component={UniversalStaticSelectField}
                label={t('rating_label_14')}
                list={CONST.questionnaireType.list}
              />
            </FieldWrap>

            <FieldWrap>
              <ButtonPrimary loading={loading}>
                Отправить ответ
              </ButtonPrimary>
            </FieldWrap>
          </FormWrapper>
        </Fragment>
      </Form>
    </DetailLayout>
  )
}

SatisfactionRating.propTypes = {
  t: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired
}

export default withTranslation()(SatisfactionRating)

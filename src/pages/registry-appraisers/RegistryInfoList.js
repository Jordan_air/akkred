import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { prop, path } from 'ramda'
import * as CONST from 'constants/backend'
import styled from 'styled-components'
import dateFormat from 'helpers/dateFormat'
import {
  Table,
  TableHeader,
  TableBody,
  TableRow,
  TableCol
} from 'components/Table'

const ListCounts = styled('div')`
  font-size: 18px;
  margin-top: 25px;
`
const Count = styled('span')`
  color: ${props => props.theme.colors.grey4};
  font-weight: 500;
`
const TableContainer = styled('div')`
  text-align: start;
`

const RegistryInfoList = props => {
  const { t, data, count} = props

  return (
    <Fragment>
      <TableContainer>
        <Table>
          <TableHeader>
            <TableRow>
              <TableCol span={10}>Ф.И.О</TableCol>
              <TableCol span={8}>Место работы</TableCol>
              <TableCol span={6} align={'right'}>Схема аккредитации</TableCol>
            </TableRow>
          </TableHeader>
          <TableBody>
            {data.map(item => {
              const id = prop('id', item)
              const firstName = prop('first_name', item)
              const lastName = prop('last_name', item)
              const middleName = prop('middle_name', item)
              const job = prop('job', item)
              const typeStandard = prop('type_standard', item)

              return (
                <TableRow key={id}>
                  <TableCol span={10}>{firstName} {lastName} {middleName}</TableCol>
                  <TableCol span={8}>{job}</TableCol>
                  <TableCol span={6} align={'right'}>{typeStandard}</TableCol>
                </TableRow>
              )
            })}
          </TableBody>
        </Table>
      </TableContainer>
      <ListCounts>
        {t('registry_query_count')}: <Count>{count}</Count>
      </ListCounts>
    </Fragment>
  )
}

RegistryInfoList.propTypes = {
  t: PropTypes.func.isRequired,
  data: PropTypes.array.isRequired,
}

RegistryInfoList.defaultProps = {
  data: [],
  count: 0,
}

export default RegistryInfoList

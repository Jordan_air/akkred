import React from 'react'
import PropTypes from 'prop-types'
import { defaultTo, groupBy, head, path, pipe, prop, toPairs } from 'ramda'
import * as ROUTES from 'constants/routes'
import getTranslate from 'helpers/getTranslate'
import { withTranslation } from 'hocs/withTranslation'
import DocumentTitle from 'components/DocumentTitle'
import { DetailLayout } from 'components/Layouts'
import { Breadcrumb, BreadcrumbItem } from 'components/Breadcrumb'
import Title from 'components/Title'
import {
  Table,
  TableHeader,
  TableBody,
  TableRow,
  TableCol,
  TableGroup
} from 'components/Table'
import Link from 'components/Link'

const Vacancy = props => {
  const { t, data } = props

  const list = pipe(
    prop('results'),
    defaultTo([]),
    groupBy(path(['category', 'id'])),
    toPairs
  )(data)

  return (
    <DetailLayout>
      <DocumentTitle>{t('vacancy_title')}</DocumentTitle>

      <Breadcrumb>
        <BreadcrumbItem href={ROUTES.ROOT_PATH}>{t('nav_home')}</BreadcrumbItem>
        <BreadcrumbItem>{t('vacancy_title')}</BreadcrumbItem>
      </Breadcrumb>

      <Title>{t('vacancy_title')}</Title>

      <Table>
        <TableHeader>
          <TableRow>
            <TableCol span={2}>№</TableCol>
            <TableCol span={16}>{t('documents_name')}</TableCol>
            <TableCol span={6} />
          </TableRow>
        </TableHeader>

        {list.map(cat => {
          const [catId, docList] = cat
          const categoryName = getTranslate(pipe(head, path(['category']))(docList))

          return (
            <TableBody key={catId}>
              <TableGroup>{categoryName}</TableGroup>
              {docList.map((doc, index) => {
                const id = prop('id', doc)
                const order = index + 1
                // const file = prop('file', doc)
                const name = getTranslate(doc)
                const file = getTranslate(doc, 'file')

                return (
                  <TableRow key={id}>
                    <TableCol span={2}>{order}</TableCol>
                    <TableCol span={16}>{name}</TableCol>
                    <TableCol span={6} align={'right'}>
                      <Link
                        href={file}
                        external={true}
                        target={'_blank'}
                        fontWeight={400}>
                        {t('link')}
                      </Link>
                    </TableCol>
                  </TableRow>
                )
              })}
            </TableBody>
          )
        })}
      </Table>
      <div style={{
        marginTop: '20px'
      }}>
        <strong>{t('vacancy_description')}</strong>

      </div>
    </DetailLayout>
  )
}

Vacancy.propTypes = {
  t: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired
}

export default withTranslation()(Vacancy)

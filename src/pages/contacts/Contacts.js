import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { YMaps, Map, Placemark } from 'react-yandex-maps'
import * as ROUTES from 'constants/routes'
import { withTranslation } from 'hocs/withTranslation'
import DocumentTitle from 'components/DocumentTitle'
import { DetailLayout } from 'components/Layouts'
import { Breadcrumb, BreadcrumbItem } from 'components/Breadcrumb'
import Title from 'components/Title'

const MAP_DEFAULT_STATE = {
  center: [41.325698, 69.322263],
  zoom: 17
}

const Wrapper = styled('div')`
  
`

const InfoBox = styled('div')`
  &:not(:last-child) {
    margin-bottom: 12px;
  }
`

const InfoTitle = styled('div')`
  font-size: 18px;
  font-weight: 500;
  margin-bottom: 10px;
`

const InfoValue = styled('div')``

const MapWrapper = styled('div')`
  margin-top: 25px;
`

const StyledMap = styled(Map)`
  width: 100%;
  height: 375px;
`

const Contacts = props => {
  const { t } = props

  return (
    <DetailLayout>
      <DocumentTitle>{t('contacts_title')}</DocumentTitle>

      <Breadcrumb>
        <BreadcrumbItem href={ROUTES.ROOT_PATH}>{t('nav_home')}</BreadcrumbItem>
        <BreadcrumbItem>{t('contacts_title')}</BreadcrumbItem>
      </Breadcrumb>

      <Title>{t('contacts_title')}</Title>

      <Wrapper>
        <InfoBox>
          <InfoTitle>{t('work_time')}</InfoTitle>
          <InfoValue>9:00 - 18:00 </InfoValue>
        </InfoBox>
        <InfoBox>
          <InfoTitle>{t('dinner_time')}</InfoTitle>
          <InfoValue>13:00 - 14:00</InfoValue>
        </InfoBox>
        <InfoBox>
          <InfoTitle>{t('relax_time')}</InfoTitle>
          <InfoValue>{t('relax_day')}</InfoValue>
        </InfoBox>

        <InfoBox>
          <InfoTitle>{t('transports')}</InfoTitle>
          <InfoValue>{t('transports_cars')}</InfoValue>
        </InfoBox>

        <InfoBox>
          <InfoTitle>{t('common_email')}</InfoTitle>
          <InfoValue>{t('contacts_email')}</InfoValue>
        </InfoBox>
        <InfoBox>
          <InfoTitle>{t('common_phone')}</InfoTitle>
          <InfoValue>{t('header_phone')}</InfoValue>
        </InfoBox>
        <InfoBox>
          <InfoTitle>{t('common_address')}</InfoTitle>
          <InfoValue>{t('contacts_address')}</InfoValue>
        </InfoBox>

        <InfoBox>
          <InfoTitle>{t('contacts_online_email')}</InfoTitle>
          <InfoValue>ariza@akkred.uz</InfoValue>
        </InfoBox>

        <MapWrapper>
          <YMaps>
            <StyledMap defaultState={MAP_DEFAULT_STATE}>
              <Placemark defaultGeometry={[41.325698, 69.322263]} />
            </StyledMap>
          </YMaps>
        </MapWrapper>
      </Wrapper>
    </DetailLayout>
  )
}

Contacts.propTypes = {
  t: PropTypes.func.isRequired
}

export default withTranslation()(Contacts)

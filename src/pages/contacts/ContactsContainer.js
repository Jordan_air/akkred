import React from 'react'
import Contacts from './Contacts'

const ContactsContainer = props => (
  <Contacts {...props} />
)

ContactsContainer.getInitialProps = async () => ({
  namespacesRequired: ['common']
})

export default ContactsContainer

import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { always } from 'ramda'
import { Search } from 'react-feather'
import * as API from 'constants/api'
import {
  Form,
  Field,
  AutoSave,
  IconInputField,
  UniversalMultiSelectField,
  UniversalStaticMultiSelectField
} from 'components/FormComponents'
import { FieldWrap } from 'components/StyledElements'
import { ButtonBordered } from 'components/Button'
import { RegistryFilterWrapper } from 'components/Registry'
import { registryStatus } from 'constants/backend'

const RegistryFilter = props => {
  const {
    t,
    initialValues,
    onFormChange,
    onFormReset
  } = props

  const onEnterSearch = (event, fieldValue) => {
    const fieldName = event.target.name

    event.preventDefault()

    onFormChange({ [fieldName]: fieldValue }, true)
  }

  return (
    <RegistryFilterWrapper>
      <Form initialValues={initialValues} onSubmit={always(true)}>
        <Fragment>
          <AutoSave
            debounce={300}
            onFormChange={onFormChange}
          />

          <FieldWrap>
            <ButtonBordered fullWidth={true} onClick={onFormReset}>
              {t('registry_reset_form')}
            </ButtonBordered>
          </FieldWrap>

          <FieldWrap>
            <Field
              name={'typeOrgan'}
              component={UniversalMultiSelectField}
              label={t('registry_organ_type')}
              api={API.TYPE_ORGAN_LIST}
              itemText={['title']}
            />
          </FieldWrap>

          <FieldWrap>
            <Field
              name={'status'}
              component={UniversalStaticMultiSelectField}
              label={t('registry_status')}
              list={registryStatus.list}
            />
          </FieldWrap>

          <FieldWrap>
            <Field
              name={'region'}
              component={UniversalMultiSelectField}
              label={t('registry_region')}
              api={API.REGION_LIST}
              itemText={['title']}
            />
          </FieldWrap>

          <FieldWrap>
            <Field
              name={'searchNumber'}
              component={IconInputField}
              label={t('registry_number')}
              placeholder={t('search')}
              icon={Search}
              onEnter={onEnterSearch}
            />
          </FieldWrap>

          <FieldWrap>
            <Field
              name={'searchTitle'}
              component={IconInputField}
              label={t('registry_legal_title')}
              placeholder={t('search')}
              icon={Search}
              onEnter={onEnterSearch}
            />
          </FieldWrap>

          <FieldWrap>
            <Field
              name={'searchInn'}
              component={IconInputField}
              label={t('registry_detail_inn')}
              placeholder={t('search')}
              icon={Search}
              onEnter={onEnterSearch}
            />
          </FieldWrap>

          <FieldWrap>
            <Field
              name={'searchCode'}
              component={IconInputField}
              label={t('regularity_docs')}
              placeholder={t('search')}
              icon={Search}
              onEnter={onEnterSearch}
            />
          </FieldWrap>

          <FieldWrap>
            <Field
              name={'search'}
              component={IconInputField}
              label={t('key_words')}
              placeholder={t('search')}
              icon={Search}
              onEnter={onEnterSearch}
            />
          </FieldWrap>

        </Fragment>
      </Form>
    </RegistryFilterWrapper>
  )
}

RegistryFilter.propTypes = {
  t: PropTypes.func.isRequired,
  initialValues: PropTypes.object.isRequired,
  onFormChange: PropTypes.func.isRequired,
  onFormReset: PropTypes.func.isRequired,
  regionList: PropTypes.array.isRequired,
  organTypeList: PropTypes.array.isRequired,
}

export default RegistryFilter

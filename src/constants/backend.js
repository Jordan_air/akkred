/* eslint-disable max-len */
import { fromPairs, map, pipe, values } from 'ramda'
import * as ROUTES from './routes'
import { sprintf } from 'sprintf-js'

const arrayObjToObj = pipe(map(values), fromPairs)

const getFormattedListData = list => ({
  list,
  object: arrayObjToObj(list)
})

export const LIVE_CHAT_SCRIPT = 'window.replainSettings = { id: \'44c2b194-456b-47fd-af69-90def40b6c2b\' };\n(function(u){var s=document.createElement(\'script\');s.type=\'text/javascript\';s.async=true;s.src=u;\nvar x=document.getElementsByTagName(\'script\')[0];x.parentNode.insertBefore(s,x);\n})(\'https://widget.replain.cc/dist/client.js\');'
export const YANDEX_METRIC = ' (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};\n' +
  '   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})\n' +
  '   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");\n' +
  '\n' +
  '   ym(72682030, "init", {\n' +
  '        clickmap:true,\n' +
  '        trackLinks:true,\n' +
  '        accurateTrackBounce:true\n' +
  '   });'
export const registryStatus = getFormattedListData([
  { id: 'active', title: 'registry_status_active' },
  { id: 'inactive', title: 'registry_status_inactive' },
  { id: 'paused', title: 'registry_status_paused' },
  // { id: 'extended', title: 'registry_status_extended' },
  { id: 'expired', title: 'registry_status_expired' }
])

export const registryInfoStatus = getFormattedListData([
  { id: 'active', title: 'registry_info_status_active' },
  { id: 'inactive', title: 'registry_info_status_inactive' },
  { id: 'paused', title: 'registry_info_status_paused' },
  { id: 'extended', title: 'registry_info_status_extended' }
])

export const registryAuthtatus = getFormattedListData([
  { id: 'accreditation', title: 'akkreditatsiya' },
  { id: 'ok', title: 'ma\'qullash' },
])

export const activityType = getFormattedListData([
  { id: 'testing_laboratory', title: 'rating_testing_laboratory' },
  { id: 'calibration_laboratory', title: 'rating_calibration_laboratory' },
  { id: 'pov_calibration_laboratory', title: 'rating_pov_calibration_laboratory' },
  { id: 'medical_laboratory', title: 'rating_medical_laboratory' },
  { id: 'product_certification_body', title: 'rating_product_certification_body' },
  { id: 'authority_for_certification_of_services', title: 'rating_authority_for_certification_of_services' },
  { id: 'management_system_certification_body', title: 'rating_management_system_certification_body' },
  { id: 'personnel_certification_body', title: 'rating_personnel_certification_body' },
  { id: 'inspection_body', title: 'rating_inspection_body' },
  { id: 'other', title: 'rating_other' }
])

export const workingType = getFormattedListData([
  { id: 'less_one_year', title: 'rating_less_one_year' },
  { id: 'more_one_year', title: 'rating_more_one_year' }
])

export const markType = getFormattedListData([
  { id: '1', title: '1' },
  { id: '2', title: '2' },
  { id: '3', title: '3' },
  { id: '4', title: '4' },
  { id: '5', title: '5' }
])

export const sourceType = getFormattedListData([
  { id: 'web_site', title: 'rating_web_site' },
  { id: 'phone', title: 'rating_phone' },
  { id: 'facebook', title: 'rating_facebook' },
  { id: 'telegram', title: 'rating_telegram' },
  { id: 'friends', title: 'rating_friends' },
  { id: 'other_source', title: 'rating_other' }
])

export const employeeAttitudeType = getFormattedListData([
  { id: 'yes', title: 'rating_yes' },
  { id: 'no', title: 'rating_no' }
])

export const questionnaireType = getFormattedListData([
  { id: 'manager', title: 'rating_manager' },
  { id: 'employee', title: 'rating_employee' },
  { id: 'other_employee', title: 'rating_other' }
])

export const calculateService = getFormattedListData([
  { id: 'testing_lab', title: 'rating_testing_laboratory', active: true, asHref : sprintf(ROUTES.CALCULATE_ITEM_URL, 'testing_lab') },
  { id: 'galibration_laboratories', title: 'galibration_laboratories', active: true, asHref : sprintf(ROUTES.CALCULATE_ITEM_URL, 'galibration_laboratories'), },
  { id: 'verification_laboratories', title: 'verification_laboratories', active: true, asHref : sprintf(ROUTES.CALCULATE_ITEM_URL, 'verification_laboratories'), },
  { id: 'product_certification', title: 'rating_product_certification_body', active: true, asHref : sprintf(ROUTES.CALCULATE_ITEM_URL, 'product_certification'), },
  { id: 'poi', title: 'poi', active: true, asHref : sprintf(ROUTES.CALCULATE_ITEM_URL, 'poi'), },
  { id: 'ospers', title: 'ospers', active: true, asHref : sprintf(ROUTES.CALCULATE_ITEM_URL, 'ospers'), },
  { id: 'ossm', title: 'ossm', active: true, asHref : sprintf(ROUTES.CALCULATE_ITEM_URL, 'ossm'), },
  // { id: 'ok', title: 'ok', active: true, asHref : sprintf(ROUTES.CALCULATE_ITEM_URL, 'ok'), },
  // { id: 'inspection_body', title: 'rating_inspection_body', active: true, asHref : sprintf(ROUTES.CALCULATE_ITEM_URL, 'inspection_body'), },
  // { id: 'management_system_certification', title: 'rating_management_system_certification_body', active: false },
  // { id: 'metro_calibration_service', title: 'rating_metrological_calibration_service', active: false },
])

export const calculateRateType = getFormattedListData([
  { id: 'accreditation', title: 'Аккредитация' },
  { id: 'expansion', title: 'Расширение' },
  { id: 'actualization', title: 'Актуализация' },
  { id: 'inspection_control', title: 'Инспекционный контроль' }
])

export const calculateRateOkType = getFormattedListData([
  { id: 'odobrenie', title: 'Одобрение' },
  { id: 'expansion', title: 'Расширение' },
  { id: 'actualization', title: 'Актуализация' },
])

export const calculateServiceType = getFormattedListData([
  { id: 'expertise', title: 'Экспертиза' },
  { id: 'site', title: 'Оценка на месте' }
])

export const applicationStatusStatus = getFormattedListData([
  { id: 'draft', title: 'qoralama' },
  { id: 'archive', title: 'arxiv' },
  { id: 'reject', title: 'Rad etilgan' },
  { id: 'new_send_center', title: 'Yangi' },
  { id: 'new_register_application', title: 'Ariza ko\'rib  chiqish jarayonida' },
  { id: 'new_create_analysis_application', title: 'Ariza ko\'rib  chiqish jarayonida' },
  { id: 'new_choice_executor', title: 'Ariza ko\'rib  chiqish jarayonida' },
  { id: 'new_fill_up_analysis_application', title: 'Ariza ko\'rib  chiqish jarayonida' },
  { id: 'new_create_contract', title: 'Ariza ko\'rib  chiqish jarayonida' },
  { id: 'new_confirm_by_department', title: 'Ariza ko\'rib  chiqish jarayonida' },
  { id: 'new_confirm_by_account', title: 'Ariza ko\'rib  chiqish jarayonida' },
  { id: 'new_sign_by_center', title: 'Ariza ko\'rib  chiqish jarayonida' },
  { id: 'new_sign_by_client', title: 'Ariza ko\'rib  chiqish jarayonida' },
  { id: 'new_pay_by_client', title: 'To`lov' },
  { id: 'new_confirm_payment', title: 'To’lov jarayoni' },
  { id: 'expertise_choice_experts', title: 'Ekspertiza jarayonida' },
  { id: 'expertise_confirm_experts_department', title: 'Ekspertiza jarayonida' },
  { id: 'expertise_confirm_experts_hr', title: 'Ekspertiza jarayonida' },
  { id: 'expertise_request_to_experts', title: 'Ekspertiza jarayonida' },
  { id: 'expertise_confirm_experts_center', title: 'Ekspertiza jarayonida' },
  { id: 'expertise_sign_experts_client', title: 'Ekspertiza jarayonida' },
  { id: 'expertise_expertise_start', title: 'Ekspertiza jarayonida' },
  { id: 'expertise_create_contract_audit', title: 'Ekspertiza jarayonida' },
  { id: 'expertise_confirm_contract_audit_department', title: 'Ekspertiza jarayonida' },
  { id: 'expertise_confirm_contract_audit_account', title: 'Ekspertiza jarayonida' },
  { id: 'expertise_sign_contract_audit_center', title: 'Ekspertiza jarayonida' },
  { id: 'expertise_sign_contract_audit_client', title: 'Ekspertiza jarayonida' },
  { id: 'expertise_pay_contract_audit_client', title: 'To’lov' },
  { id: 'expertise_confirm_payment_contract_audit', title: 'To’lov jarayoni' },
  { id: 'audit_choice_experts', title: 'Baholash rejasini rasmiylashtirish' },
  { id: 'audit_confirm_experts_department', title: 'Baholash rejasini rasmiylashtirish' },
  { id: 'audit_confirm_experts_hr', title: 'Baholash rejasini rasmiylashtirish' },
  { id: 'audit_request_to_experts', title: 'Baholash rejasini rasmiylashtirish' },
  { id: 'audit_sign_plan_center', title: 'Baholash rejasini rasmiylashtirish' },
  { id: 'audit_sign_plan_client', title: 'Baholash rejasini kelishish' },
  { id: 'audit_create_order', title: 'Joyini o\'zida baholash  jarayonida' },
  { id: 'audit_accept_order', title: 'Joyini o\'zida baholash  jarayonida' },
  { id: 'audit_sign_order_center', title: 'Joyini o\'zida baholash  jarayonida' },
  { id: 'audit_start_audit', title: 'Joyini o\'zida baholash  jarayonida' },
  { id: 'audit_process_audit', title: 'Joyini o\'zida baholash  jarayonida' },
  { id: 'audit_end_audit', title: 'Joyini o\'zida baholash  jarayonida' },
  { id: 'audit_accept_audit_result', title: 'Joyini o\'zida baholash  jarayonida' },
  { id: 're_audit_start_deadline', title: 'O`rganish' },
  { id: 're_audit_create_plan', title: 'O`rganish' },
  { id: 're_audit_create_proof_plan', title: 'O`rganish' },
  { id: 're_audit_end_deadline', title: 'O`rganish' },
  { id: 're_audit_choice_experts', title: 'O`rganish' },
  { id: 're_audit_create_order', title: 'O`rganish' },
  { id: 're_audit_accept_order', title: 'O`rganish' },
  { id: 're_audit_sign_order_center', title: 'O`rganish' },
  { id: 're_audit_start_audit', title: 'O`rganish' },
  { id: 're_audit_process_audit', title: 'O`rganish' },
  { id: 're_audit_end_audit', title: 'O`rganish' },
  { id: 're_audit_accept_audit_result', title: 'O`rganish' },
  { id: 'audit_create_analysis', title: 'O`rganish' },
  { id: 'commission_send_participants', title: 'O`rganish' },
  { id: 'commission_send_participants', title: 'Akrreditatsiya komissiyasi' },
  { id: 'commission_vote_participants', title: 'Akrreditatsiya komissiyasi' },
  { id: 'commission_create_protocol', title: 'Akrreditatsiya komissiyasi' },
  { id: 'commission_sign_protocol_center', title: 'Akrreditatsiya komissiyasi' },
  { id: 'commission_register_create', title: 'Reestrda ro’yhatga olish' },
  { id: 'post_create_post', title: 'Akkreditatsiyadan keyingi shartnomani rasmiylashtirish' },
  { id: 'post_sign_post_center', title: 'Akkreditatsiyadan keyingi shartnomani rasmiylashtirish' },
  { id: 'post_sign_post_client', title: 'Akkreditatsiyadan keyingi shartnomani rasmiylashtirish' },

])

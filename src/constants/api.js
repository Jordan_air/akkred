export const API_URL = 'https://akkred.uz:8081'
// export const API_URL = 'http://localhost:8000'
// export const API_URL = 'http://217.29.126.137:8081'
export const MENU_LIST = `${API_URL}/main/menu/menu`
export const MEDIA_URL = `${API_URL}/media/`

export const SLIDER_LIST = `${API_URL}/main/sliders/`
export const REGION_LIST = `${API_URL}/main/region/`
export const STATIC_PAGE = `${API_URL}/main/static-pages/%s/`

const TYPE_ORGAN = `${API_URL}/main/type-organ`
export const TYPE_ORGAN_LIST = `${TYPE_ORGAN}/`
export const TYPE_ORGAN_STATIC_LIST = `${TYPE_ORGAN}/static/`

const COURSE = `${API_URL}/main/course`
export const COURSE_LIST = `${COURSE}-category/`

export const COURSE_CREATE = `${COURSE}/`

const CODE_TNV = `${API_URL}/main/tnv/search`
export const CODE_TNV_LIST = `${CODE_TNV}/`

const NEWS = `${API_URL}/main/news`
export const NEWS_LIST = `${NEWS}/`
export const NEWS_ITEM = `${NEWS}/%s/`

const DOCUMENTS = `${API_URL}/main/documents`
export const DOCUMENT_LIST = `${DOCUMENTS}/`
export const DOCUMENT_ITEM = `${DOCUMENTS}/%d/`

const REGISTRY = `${API_URL}/main/registries`
export const REGISTRY_LIST = `${REGISTRY}/`
export const REGISTRY_ITEM = `${REGISTRY}/%s/`

const REGISTRY_CONFIRM = `${API_URL}/main/reestr-conf`
export const REGISTRY_CONFIRM_LIST = `${REGISTRY_CONFIRM}/`
export const REGISTRY_CONFIRM_ITEM = `${REGISTRY_CONFIRM}/%s/`

const REGISTRY_AUTH = `${API_URL}/main/reestr-auth`
export const REGISTRY_AUTH_LIST = `${REGISTRY_AUTH}/`
export const REGISTRY_AUTH_ITEM = `${REGISTRY_AUTH}/%s/`

const REGISTRY_INFO = `${API_URL}/main/reestr-status`
export const REGISTRY_INFO_LIST = `${REGISTRY_INFO}/`

const SATISFY_RATING = `${API_URL}/main/sat-quest`
export const SATISFY_RATING_CREATE = `${SATISFY_RATING}/`

const MANAGEMENT = `${API_URL}/main/employees`
export const MANAGEMENT_LIST = `${MANAGEMENT}/`

const INSPECTION = `${API_URL}/main/inspection`
export const INSPECTION_LIST = `${INSPECTION}/`

const FINANCIAL = `${API_URL}/main/financial`
export const FINANCIAL_LIST = `${FINANCIAL}/`

const VACANCY = `${API_URL}/main/vacancy`
export const VACANCY_LIST = `${VACANCY}/`

const POSITION = `${API_URL}/main/positions`
export const POSITION_LIST = `${POSITION}/`

const CALCULATE = `${API_URL}/main/calculation/calculation`
export const CALCULATE_CREATE = `${CALCULATE}/`

const CALCULATE_TWO = `${API_URL}/main/calculation/calculation_two`
export const CALCULATE_CREATE_TWO = `${CALCULATE_TWO}/`

const CALCULATE_THREE = `${API_URL}/main/calculation/calculation_three`
export const CALCULATE_CREATE_THREE = `${CALCULATE_THREE}/`

const CALCULATE_FOUR = `${API_URL}/main/calculation/calculation_four`
export const CALCULATE_CREATE_FOUR = `${CALCULATE_FOUR}/`

const CALCULATE_POI = `${API_URL}/main/calculation/calculation_poi`
export const CALCULATE_CREATE_POI = `${CALCULATE_POI}/`

const CALCULATE_OSPERS = `${API_URL}/main/calculation/calculation_ospers`
export const CALCULATE_CREATE_OSPERS = `${CALCULATE_OSPERS}/`

const CALCULATE_OSSM = `${API_URL}/main/calculation/calculation_ossm`
export const CALCULATE_CREATE_OSSM = `${CALCULATE_OSSM}/`

const CALCULATE_OK = `${API_URL}/main/calculation/calculation_ok`
export const CALCULATE_CREATE_OK = `${CALCULATE_OK}/`

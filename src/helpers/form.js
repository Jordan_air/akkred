import { omit, pick, join, isEmpty, pipe, prop, is } from 'ramda'
import { FORM_ERROR } from 'final-form'

export const mapResponseToFormError = data => {
  const NON_FIELD_ERRORS = 'nonFieldErrors'

  const fieldErrors = omit([NON_FIELD_ERRORS], data)
  const commonErrors = pick([NON_FIELD_ERRORS], data)

  if (is(Error, data)) {
    throw data
  }

  if (isEmpty(commonErrors)) {
    return fieldErrors
  }

  return {
    ...fieldErrors,
    [FORM_ERROR]: join(', ', commonErrors[NON_FIELD_ERRORS])
  }
}

const toArray = err => {
  if (!err) {
    return []
  }

  if (Array.isArray(err)) {
    return err
  }

  return [err]
}

export const getFieldError = meta => {
  const touched = prop('touched', meta)
  const error = prop('error', meta)
  const submitError = prop('submitError', meta)
  const formatError = pipe(toArray, join('<br/>'))

  if (submitError) {
    return formatError(submitError)
  }

  return touched ? formatError(error) : null
}

import { curry, prop } from 'ramda'

export const replaceRouterQuery = curry((newQuery, router) => {
  const pathname = prop('pathname', router)
  const query = prop('query', router)

  return router.replace({
    pathname,
    query: { ...query, ...newQuery }
  })
})

import moment from 'moment'
import { path } from 'ramda'
import NextI18Next from '../../i18n'

export default date => {
  if (!date) return null

  const language = path(['i18n', 'language'], NextI18Next)
  moment.locale(language)
  return moment(date).format('DD MMM YYYY')
}

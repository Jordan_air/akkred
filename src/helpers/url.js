import {
  compose, pipe, join, map, toPairs,
  curry, mergeRight, filter, isEmpty,
  not, prop, path
} from 'ramda'

export const getParamFromRouter = curry((key, router) =>
  path(['query', key], router))

export const paramsToQueryString = params => {
  if (params) {
    return compose(
      s => `?${s}`,
      join('&'),
      map(join('=')),
      map(map(encodeURIComponent)),
      toPairs,
    )(params)
  }
  return ''
}

export const appendParamsToUrl = curry((appendParams, router) => {
  const pathname = prop('pathname', router)
  const query = prop('query', router)
  const newParams = pipe(
    mergeRight(query),
    filter(pipe(isEmpty, not))
  )(appendParams)

  return pathname + paramsToQueryString(newParams)
})

export default value => {
  if (value) {
    return String(value).replace(/ /g, '')
  }
  return value
}

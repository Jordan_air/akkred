import React from 'react'
import PropTypes from 'prop-types'
import { prop } from 'ramda'
import { sprintf } from 'sprintf-js'
import styled from 'styled-components'
import { useRouter } from 'next/router'
import * as ROUTES from 'constants/routes'
import getTranslate from 'helpers/getTranslate'
import { withTranslation } from 'hocs/withTranslation'
import { BackgroundImage, Datee, TextOverflow } from 'components/StyledElements'
import Button from 'components/Button'

const Wrapper = styled('div')`
  background-color: white;
  border: ${props => props.theme.borderLight};
  border-radius: 5px;
  cursor: pointer;
  overflow: hidden;
  transition: ${props => props.theme.transition};
  &:hover {
    box-shadow: ${props => props.theme.boxShadow};
  }
`

const Image = styled(BackgroundImage)`
  height: 200px;
`

const Content = styled('div')`
  padding: 20px;
`

const Title = styled(TextOverflow)`
  font-size: 20px;
  line-height: 30px;
  height: 60px;
  margin-bottom: 20px;
  transition: ${props => props.theme.transition};
  ${Wrapper}:hover & {
    color: ${props => props.theme.colors.primary};
  }
`

const NewsCard = props => {
  const { t, data } = props

  const router = useRouter()

  const id = prop('id', data)
  const createdDate = prop('created_date_by_admin', data)
  const image = prop('image_main', data)
  const title = getTranslate(data, 'title')

  const goToDetail = () => {
    const path = ROUTES.NEWS_ITEM_PATH
    const asPath = sprintf(ROUTES.NEWS_ITEM_URL, id)
    return router.push(path, asPath)
  }

  return (
    <Wrapper onClick={goToDetail}>
      <Image url={image} />
      <Content>
        <Datee margin={10}>{createdDate}</Datee>
        <Title>{title}</Title>
        <Button>{t('more')}</Button>
      </Content>
    </Wrapper>
  )
}

NewsCard.propTypes = {
  t: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
}

export default withTranslation()(NewsCard)

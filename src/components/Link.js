import React from 'react'
import PropTypes from 'prop-types'
import { Link as I18NLink } from 'components/I18N'
import styled from 'styled-components'

const StyledLink = styled('a')`
  color: ${props => props.theme.colors.primary};
  font-weight: ${props => props.fontWeight};
  text-decoration: none;
  transition: ${props => props.theme.transition};
`

const Link = props => {
  const { href, asHref, children, external, ...restProps } = props

  if (external) {
    return (
      <StyledLink href={href} {...restProps}>
        {children}
      </StyledLink>
    )
  }

  return (
    <I18NLink href={href} as={asHref} passHref={true}>
      <StyledLink {...restProps}>
        {children}
      </StyledLink>
    </I18NLink>
  )
}

Link.propTypes = {
  href: PropTypes.string.isRequired,
  asHref: PropTypes.string,
  children: PropTypes.node,
  fontWeight: PropTypes.number,
  external: PropTypes.bool,
}

Link.defaultProps = {
  fontWeight: 500
}

export default Link

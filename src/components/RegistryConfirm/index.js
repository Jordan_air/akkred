import { always, head, includes, join, map, mapObjIndexed, pipe, prop, toPairs } from 'ramda'
import scrollToTop from 'helpers/scrollToTop'
import { replaceRouterQuery } from 'helpers/router'

export { default as RegistryDetail } from './RegistryDetail'
export { default as RegistryRow } from './RegistryRow'
export { default as RegistryFilterWrapper } from './RegistryFilterWrapper'

export const onFormReset = (router, initialValues) => {
  const urlParams = pipe(
    mapObjIndexed(always(null))
  )(initialValues)

  scrollToTop()
  replaceRouterQuery({ page: 1, ...urlParams }, router)
}

export const onFormChange = (value, router, isForced) => {
  const disableAutoSaveFields = [
    'searchNumber',
    'searchTitle',
    'search',
    'searchCode',
    'searchInn',
  ]
  const arrayValue = pipe(toPairs, head)(value)
  const [fieldName, fieldValue] = arrayValue

  if (includes(fieldName, disableAutoSaveFields) && !isForced) {
    return null
  }

  // if (not(includes(fieldName, disableScrollFields))) {
  // scrollToTop()
  // }

  if (Array.isArray(fieldValue)) {
    const formedValue = pipe(
      map(prop('id')),
      join('-')
    )(fieldValue)

    return replaceRouterQuery({ page: 1, [fieldName]: formedValue }, router)
  }

  return replaceRouterQuery({ page: 1, ...value }, router)
}

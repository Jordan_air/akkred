import React from 'react'
import PropTypes from 'prop-types'
import { prop } from 'ramda'
import styled from 'styled-components'
import dateFormat from 'helpers/dateFormat'
import getTranslate from 'helpers/getTranslate'
import {
  Table,
  TableBody,
  TableRow,
  TableCol
} from 'components/Table'

const Wrapper = styled('div')``

const SimpleCol = styled(TableCol)`
  text-align: left;
  &:first-child {
    padding-left: 32px;
  }
  &:last-child {
    padding-right: 32px;
  }
`

const ExtLink = styled('a')`
  color: ${props => props.theme.colors.primary};
  text-decoration: none;
`

const RegistryDetail = props => {
  const { t, data } = props

  const id = prop('id', data)
  const area = prop('area', data)
  const titleOrgan = prop('title_organ', data)
  const titleYurdLisa = prop('title_yurd_lisa', data)
  const inn = prop('inn', data)
  const phone = prop('phone', data)
  const email = prop('email', data)
  const webSite = prop('web_site', data)
  const fullNameSupervisorAo = prop('full_name_supervisor_ao', data)
  const addressOrgan = prop('address_organ', data)
  const fileOblast = prop('file_oblast', data)
  const reissueDate = prop('reissue_date', data)
  const is_reissue_date = prop('is_reissue_date', data)
  const titleOrganType = prop('title_organ_type', data)
  const statusDate = prop('status_date', data)

  return (
    <Wrapper>
      <Table gutter={32}>
        <TableBody>
          <TableRow>
            <SimpleCol span={10}>{t('registry_confirm_table_legal')}</SimpleCol>
            <SimpleCol span={14}>{titleOrgan} {titleOrganType} {titleYurdLisa}</SimpleCol>
          </TableRow>
          <TableRow>
            <SimpleCol span={10}>{t('registry_confirm_detail_inn')}</SimpleCol>
            <SimpleCol span={14}>{inn}</SimpleCol>
          </TableRow>

          <TableRow>
            <SimpleCol span={10}>{t('registry_confirm_detail_phone')}</SimpleCol>
            <SimpleCol span={14}>{phone}</SimpleCol>
          </TableRow>
          <TableRow>
            <SimpleCol span={10}>{t('registry_confirm_detail_email')}</SimpleCol>
            <SimpleCol span={14}>{email}</SimpleCol>
          </TableRow>
          <TableRow>
            <SimpleCol span={10}>{t('registry_confirm_detail_website')}</SimpleCol>
            <SimpleCol span={14}>{webSite}</SimpleCol>
          </TableRow>
          <TableRow>
            <SimpleCol span={10}>{t('registry_confirm_detail_supervisor_name')}</SimpleCol>
            <SimpleCol span={14}>{fullNameSupervisorAo}</SimpleCol>
          </TableRow>
          <TableRow>
            <SimpleCol span={10}>{t('registry_confirm_detail_accreditation_address')}</SimpleCol>
            <SimpleCol span={14}>{addressOrgan}</SimpleCol>
          </TableRow>

          <TableRow>
            <SimpleCol span={10}>{t('reissued_date')}</SimpleCol>
            <SimpleCol span={14}>{reissueDate}</SimpleCol>
          </TableRow>
          <TableRow>
            <SimpleCol span={10}>{t('certificate')}</SimpleCol>
            <SimpleCol span={14}><ExtLink href={`https://akkred.uz:8081/main/reestr-conf/${area}/pdf/`} rel={'noopener noreferrer'} target={'_blank'}>
              {t('download_file')}
            </ExtLink></SimpleCol>
          </TableRow>
          <TableRow>
            <SimpleCol span={10}>{t('registry_confirm_detail_accreditation_link')}</SimpleCol>
            <SimpleCol span={14}><ExtLink href={fileOblast} rel={'noopener noreferrer'} target={'_blank'}>
              {t('download_file')}
            </ExtLink></SimpleCol>
          </TableRow>
          <TableRow>
            <SimpleCol span={10}>{t('registry_confirm_date_status')}</SimpleCol>
            <SimpleCol span={14}>{statusDate}</SimpleCol>
          </TableRow>
        </TableBody>
      </Table>
    </Wrapper>
  )
}

RegistryDetail.propTypes = {
  t: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
}

export default RegistryDetail

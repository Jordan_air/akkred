import styled, { css } from 'styled-components'
import CheckMark from './CheckMark'

export default styled('label')`
  color: ${props => props.checked ? props.theme.colors.primary : props.theme.colors.text};
  cursor: pointer;
  font-size: 14px;
  display: block;
  line-height: 20px;
  margin-bottom: 12px;
  padding-left: 28px;
  position: relative;
  user-select: none;
  width: fit-content;
  &:last-child {
    margin-bottom: 0;
  }
  &:hover ${CheckMark} {
    border-color: ${props => props.theme.colors.primary};
  }
  ${props => props.disabled && (
    css`
      opacity: 0.6;
      pointer-events: none;
    `
  )
}
`

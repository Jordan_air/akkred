import React from 'react'
import PropTypes from 'prop-types'
import { path } from 'ramda'
import styled from 'styled-components'

const Fieldset = styled('fieldset')`
  border: none;
  padding: 0;
`

const RadioGroup = props => {
  const { children, onChange, name, value, type, error, resetMargin } = props

  const onClick = event => {
    if (typeof onChange === 'function') {
      onChange(event.target.value, event)
    }
  }

  const style = { margin: (resetMargin || error) ? '0' : '0 0 -12px 0' }

  return (
    <Fieldset name={name} onChange={onClick} style={style}>
      {React.Children.map(children, (child, key) => {
        const childValue = path(['props', 'value'], child)
        const checked = childValue === value
        return (
          <child.type
            key={key}
            name={name}
            checked={checked}
            onChange={() => null}
            type={type}
            {...child.props}
          />
        )
      })}
    </Fieldset>
  )
}

RadioGroup.propTypes = {
  onChange: PropTypes.func,
  name: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string
  ]),
  type: PropTypes.oneOf(['block', 'inline']),
  resetMargin: PropTypes.bool,
  error: PropTypes.string,
  children: PropTypes.node.isRequired
}

RadioGroup.defaultProps = {
  resetMargin: false,
  type: 'block'
}

export default RadioGroup

import React, { Fragment, useState } from 'react'
import PropTypes from 'prop-types'
import { slice } from 'ramda'
import styled from 'styled-components'
import { withTranslation } from 'hocs/withTranslation'

const MoreBtn = styled('div')`
  color: #93a2b3;
  cursor: pointer;
  display: inline-block;
  margin-top: 15px;
`

const MoreButton = withTranslation()(props => {
  const { t, onClick, isExpanded } = props

  return (
    <MoreBtn onClick={onClick}>
      {isExpanded ? t('show_less') : t('show_more')}
    </MoreBtn>
  )
})

const ShowMore = props => {
  const { children } = props

  const [isExpanded, setExpanded] = useState(false)

  const arrayChildren = React.Children.toArray(children)
  const arrayToMap = isExpanded ? arrayChildren : slice(0, 3, arrayChildren)

  const onToggleExpand = () => setExpanded(!isExpanded)

  return (
    <Fragment>
      <Fragment>
        {arrayToMap.map((child, key) => {
          return (
            <child.type
              key={key}
              {...child.props}
            />
          )
        })}
      </Fragment>

      <MoreButton
        onClick={onToggleExpand}
        isExpanded={isExpanded}
      />
    </Fragment>
  )
}

ShowMore.propTypes = {
  children: PropTypes.node.isRequired
}

MoreButton.propTypes = {
  t: PropTypes.func,
  isExpanded: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
}

export { MoreButton }

export default ShowMore

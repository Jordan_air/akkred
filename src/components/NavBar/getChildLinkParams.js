import { always, drop, filter, head, join, map, pipe, split } from 'ramda'

export default (url, external) => {
  const splittedUrl = pipe(split('/'), filter(Boolean))(url)
  const mainPathname = head(splittedUrl)
  const slugs = pipe(
    drop(1),
    map(always('[slug]')),
    join('/')
  )(splittedUrl)
  const hasSlug = splittedUrl.length > 1 && !external
  const formedHref = `/${mainPathname}/${slugs}`
  const href = hasSlug ? formedHref : url
  const asHref = hasSlug ? url : ''

  return {
    href,
    asHref
  }
}

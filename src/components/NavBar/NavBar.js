import React from 'react'
import styled from 'styled-components'
import { ROOT_PATH } from 'constants/routes'
import { devices } from 'constants/mediaQueries'
import Link from 'components/Link'
import Container from 'components/Container'
import Logo from './Logo'
import Navigation from './Navigation'
import MobileNavigation from './MobileNavigation'

const Wrapper = styled('div')`
  background-color: ${props => props.theme.colors.primary};
  box-shadow: 0 5px 10px rgba(0, 0, 0, 0.25);
  color: white;
  height: 80px;
  & ${Container} {
    height: 100%;
  }
`

const Nav = styled('div')`
  align-items: center;
  display: flex;
  height: 100%;
  position: relative;
`

const LogoWrap = styled('div')`
  transform-origin: left;
  transform: scale(0.8);
  @media (min-width: ${devices.tabletL}) {
    margin-right: 30px;
  }
  @media (min-width: ${devices.laptopS}) {
    transform: none;
  }
`

const NavBar = () => {
  const linkStyle = { display: 'block' }

  return (
    <Wrapper>
      <Container>
        <Nav>
          <LogoWrap>
            {/*<Link href={ROOT_PATH} style={linkStyle}>*/}
              <Logo />
            {/*</Link>*/}
          </LogoWrap>

          <Navigation />
          <MobileNavigation />
        </Nav>
      </Container>
    </Wrapper>
  )
}

NavBar.propTypes = {}

export default NavBar

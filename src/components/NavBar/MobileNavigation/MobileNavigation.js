import React, { useContext, useState, useEffect } from 'react'
import styled from 'styled-components'
import { Menu, X } from 'react-feather'
import { devices } from 'constants/mediaQueries'
import AppContext from 'components/Contexts/AppContext'
import Languages from 'components/Header/Languages'
import Portal from './Portal'
import NavigationLink from './NavigationLink'

const Wrapper = styled('div')`
  display: block;
  position: absolute;
  top: 50%;
  right: 0;
  transform: translateY(-50%);
  @media (min-width: ${devices.laptopM}) {
    display: none;
  }
`

const IconWrap = styled('div')`
  padding: 10px;
  cursor: pointer;
  & > svg {
    display: block;
  }
`

const CloseIconWrap = styled(IconWrap)`
  position: absolute;
  top: 15px;
  right: 15px;
  & > svg {
    color: ${props => props.theme.colors.text};
  }
`

const Drawer = styled('div')`
  background-color: white;
  box-shadow: -4px 0 15px 0 rgba(0, 0, 0, 0.15);
  display: flex;
  flex-direction: column;
  position: fixed;
  top: 0;
  right: 0;
  height: 100vh;
  max-width: 500px;
  opacity: ${props => props.isOpen ? '1' : '0'};
  padding: 74px 30px 30px;
  transition: opacity 100ms, transform 150ms ease-out;
  transform: translateX(${props => props.isOpen ? '0' : '100%'});
  width: 100%;
  z-index: 99999999999;
`

const NavWrap = styled('div')`
  border-bottom: ${props => props.theme.borderLight};
  flex-grow: 1;
  margin: 0 -30px;
  overflow-x: hidden;
  overflow-y: auto;
  padding: 0 30px 30px;
`

const Footer = styled('div')`
  align-items: center;
  display: flex;
  justify-content: space-between;
  padding-top: 30px;
`

const LangWrap = styled('div')`
  align-items: center;
  display: flex;
`

export default () => {
  const [open, setOpen] = useState(false)
  const { menu } = useContext(AppContext)

  const onOpenMenu = () => setOpen(true)
  const onCloseMenu = () => setOpen(false)

  const homeMenu = {
    title_ru: 'Главная',
    title_uz: 'Bosh sahifa',
    title_en: 'Home',
    url: '/'
  }

  const fullMenu = [homeMenu, ...menu]

  useEffect(() => {
    const HTML = document.querySelector('html')
    const BODY = document.body
    const SCROLLBAR_WIDTH = window.innerWidth - document.documentElement.clientWidth
    if (open) {
      HTML.style.overflow = 'hidden'
      BODY.style.overflow = 'hidden'
      BODY.style.paddingRight = `${SCROLLBAR_WIDTH}px`
    } else {
      HTML.style = null
      BODY.style = null
    }
  }, [open])

  return (
    <Wrapper>
      <IconWrap onClick={onOpenMenu}>
        <Menu />
      </IconWrap>

      <Portal>
        <Drawer isOpen={open}>
          <CloseIconWrap onClick={onCloseMenu}>
            <X />
          </CloseIconWrap>

          <NavWrap>
            {fullMenu.map((item, index) => (
              <NavigationLink
                {...item}
                key={index}
                onCloseMenu={onCloseMenu}
              />
            ))}
          </NavWrap>

          <Footer>
            <Languages as={LangWrap} />
          </Footer>
        </Drawer>
      </Portal>
    </Wrapper>
  )
}

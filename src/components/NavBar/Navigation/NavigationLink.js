import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { isEmpty, not } from 'ramda'
import styled from 'styled-components'
import { ChevronRight } from 'react-feather'
import getTranslate from 'helpers/getTranslate'
import { withTranslation } from 'hocs/withTranslation'
import getChildLinkParams from '../getChildLinkParams'
import NavigationSubMenu from './NavigationSubMenu'
import {
  NavLink,
  NavLinkChild,
  NavLinkWrap,
  NavLinkWrapChild,
  NavWrapper
} from './Components'

const NavChevron = styled(ChevronRight)`
  position: absolute;
  top: 50%;
  right: 5px;
  transform: translateY(-50%);
`

const NavigationLink = props => {
  const {
    t,
    url,
    external,
    children,
    level,
    isChild
  } = props

  const [isVisible, setIsVisible] = useState(false)

  const onMouseOver = () => {
    setIsVisible(true)
  }
  const onMouseLeave = () => {
    setIsVisible(false)
  }

  const navTitle = getTranslate(props, 'title')
  const isTopLevel = level === 0
  const linkExtraProps = external ? { target: '_blank' } : {}

  if (not(isEmpty(children))) {
    return (
      <NavWrapper
        isTopLevel={isTopLevel}
        onMouseOver={onMouseOver}
        onMouseLeave={onMouseLeave}>
        {isChild
          ? (
            <NavLinkWrapChild title={navTitle}>
              {navTitle}
              <NavChevron size={16} />
            </NavLinkWrapChild>
          )
          : (
            <NavLinkWrap title={navTitle}>
              {navTitle}
            </NavLinkWrap>
          )}
        {isVisible && (
          <NavigationSubMenu isTopLevel={isTopLevel}>
            {children.map((item, index) => {
              return (
                <NavigationLink
                  {...item}
                  t={t}
                  key={index}
                  level={level + 1}
                  isChild={true}
                />
              )
            })}
          </NavigationSubMenu>
        )}
      </NavWrapper>
    )
  }

  if (isChild) {
    const { href, asHref } = getChildLinkParams(url, external)

    return (
      <NavLinkChild
        href={href}
        asHref={asHref}
        external={external}
        title={navTitle}
        {...linkExtraProps}>
        {navTitle}
      </NavLinkChild>
    )
  }

  return (
    <NavLink href={url}>
      {navTitle}
    </NavLink>
  )
}

NavigationLink.propTypes = {
  t: PropTypes.func.isRequired,
  url: PropTypes.string,
  external: PropTypes.bool,
  children: PropTypes.array,
  level: PropTypes.number,
  isChild: PropTypes.bool,
}

NavigationLink.defaultProps = {
  level: 0,
  external: false,
  url: '',
  children: []
}

export default withTranslation()(NavigationLink)

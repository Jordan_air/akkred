import styled from 'styled-components'
import Link from 'components/Link'

const topLevelLinkStyles = {
  alignItems: 'center',
  color: 'inherit',
  display: 'inline-flex',
  fontSize: '17px',
  fontWeight: 'normal',
  height: '100%',
  transition: props => props.theme.transition,
  '&:not(:last-child)': {
    marginRight: '30px'
  }
}

const childLinkStyles = {
  color: props => props.theme.colors.text,
  display: 'block',
  fontSize: '14px',
  fontWeight: 'normal',
  lineHeight: '20px',
  padding: '10px',
  textOverflow: 'ellipsis',
  transition: props => props.theme.transition,
  overflow: 'hidden',
  whiteSpace: 'nowrap',
  '&:hover': {
    backgroundColor: props => props.theme.colors.grey,
    color: props => props.theme.colors.primary
  }
}

export const NavLink = styled(Link)({
  ...topLevelLinkStyles,
  fontSize: '17px'
})

export const NavLinkChild = styled(Link)({
  ...childLinkStyles
})

export const NavWrapper = styled('div')({
  ...topLevelLinkStyles,
  cursor: 'pointer',
  position: 'relative',
  width: props => props.isTopLevel ? 'unset' : '100%'
})

export const NavLinkWrap = styled('div')``

export const NavLinkWrapChild = styled('div')({
  ...childLinkStyles,
  padding: '10px 26px 10px 10px',
  position: 'relative',
  width: '100%'
})

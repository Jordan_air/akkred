import PropTypes from 'prop-types'
import styled from 'styled-components'

const BgImage = styled('div')`
  background-image: ${props => props.url ? `url(${props.url})` : 'none'};
  background-position: ${props => props.backgroundPosition};
  background-size: ${props => props.backgroundSize};
  background-repeat: no-repeat;
`

BgImage.propTypes = {
  url: PropTypes.string,
  backgroundPosition: PropTypes.string,
  backgroundSize: PropTypes.string,
}

BgImage.defaultProps = {
  backgroundPosition: 'center',
  backgroundSize: 'cover'
}

export default BgImage

import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import dateFormat from 'helpers/dateFormat'

const Datee = styled('div')`
  color: rgba(48, 68, 89, 0.7);
  font-size: 16px;
  margin-bottom: ${props => props.margin}px; 
`

const DateComponent = props => {
  const { children, ...restProps } = props

  const date = dateFormat(children)

  return (
    <Datee {...restProps}>{date}</Datee>
  )
}

DateComponent.propTypes = {
  margin: PropTypes.number,
  children: PropTypes.node,
}

export default DateComponent

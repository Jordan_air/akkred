import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const Text = styled('div')`
  display: -webkit-box;
  overflow: hidden;
  text-overflow: ellipsis;
  -webkit-line-clamp: ${props => props.limit};
  -webkit-box-orient: vertical;
`

const TextOverflow = props => {
  const { limit, ...restProps } = props

  return (
    <Text limit={limit} {...restProps} />
  )
}

TextOverflow.propTypes = {
  limit: PropTypes.number
}

TextOverflow.defaultProps = {
  limit: 2
}

export default TextOverflow

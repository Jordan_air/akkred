import React from 'react'
import styled from 'styled-components'
import Telegram from './icons/Telegram'
import Instagram from './icons/Instagram'
import Facebook from './icons/Facebook'
import View from './icons/View'
import Link from 'components/Link'

const Wrapper = styled('div')`
  align-items: center;
  display: flex;
`

const StyledLink = styled(Link)`
  &:not(:last-child) {
    margin-right: 15px;
  }
  & > svg {
    color: ${(props) => props.theme.colors.grey4};
    display: block;
  }
`

const socials = [
  {
    id: 'telegram',
    link: 'https://t.me/uzakkreditatsiya',
    icon: <Telegram />,
  },
  {
    id: 'instagram',
    link: 'https://instagram.com',
    icon: <Instagram />,
  },
  {
    id: 'facebook',
    link: 'https://www.facebook.com/Akkreditatsiya-Markazi-108530407219743',
    icon: <Facebook />,
  },
  {
    id: 'view',
    link: 'http://finevision.ru/?hostname=akkred.uz&path=/',
    icon: <View />,
  },
]

export default () => {
  return (
    <Wrapper>
      {socials.map((item) => {
        return (
          <StyledLink key={item.id} href={item.link} external={true} target={'_blank'}>
            {item.icon}
          </StyledLink>
        )
      })}
    </Wrapper>
  )
}

import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Languages from './Languages'
import Socials from './Socials'
import { devices } from 'constants/mediaQueries'
import { withTranslation } from 'hocs/withTranslation'
import Container from 'components/Container'
import NavBar from 'components/NavBar'
import Search from '../Search'
// function debounce (func, interval) {
//   let timer
//   return function () {
//     clearTimeout(timer)
//     const args = arguments
//     const that = this
//     timer = setTimeout(function () {
//       func.apply(that, args)
//     }, interval)
//   }
// }

const Root = styled('div')`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  z-index: 100;
`

const HeaderRoot = styled('div')`
  background-color: white;
  display: none;
  @media (min-width: ${devices.laptopS}) {
    display: block;
  }
`

const Flex = styled('div')`
  align-items: center;
  display: flex;
`

const LangWrap = styled('div')`
  margin-left: 90px;
`

const Wrapper = styled(Flex)`
  justify-content: space-between;
  height: 45px;
`

const InfoTitle = styled('div')`
  font-size: 16px;
`

const Address = styled('div')`
  display: none;
  margin-right: 25px;
  @media (min-width: ${devices.laptopM}) {
    display: block;
  }
`

const Phone = styled('div')`
  margin-right: 25px;
`

const Header = (props) => {
  const { t } = props

  return (
    <Fragment>
      <Root>
        <HeaderRoot>
          <Container>
            <Wrapper>
              <Flex>
                <InfoTitle>{t('header_title')}</InfoTitle>
                <LangWrap>
                  <Languages />
                </LangWrap>
              </Flex>

              <Flex className={'info'}>
                <Address>{t('header_address')}</Address>
                <Phone>{t('header_phone')}</Phone>
                <Socials />
              </Flex>
              <Flex>
                <Search />
              </Flex>
            </Wrapper>
          </Container>
        </HeaderRoot>

        <NavBar />
      </Root>
    </Fragment>
  )
}

Header.propTypes = {
  t: PropTypes.func.isRequired,
}

export default withTranslation()(Header)

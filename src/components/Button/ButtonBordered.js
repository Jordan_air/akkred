import styled from 'styled-components'
import ButtonBase from './ButtonBase'

export default styled(ButtonBase)`
  background-color: white;
  border: 1px solid;
  color: ${props => props.theme.colors.primary};
  font-size: 15px;
  height: 40px;
  padding: 0 40px;
`

import styled from 'styled-components'
import { devices } from 'constants/mediaQueries'

export default styled('div')`
  border: ${props => props.theme.border};
  border-radius: ${props => props.theme.borderRadius};
  margin-bottom: 30px;
  padding: 38px 26px;
  @media (min-width: ${devices.laptopS}) {
    margin-bottom: 0;
    margin-right: 50px;
  }
`

import React from 'react'
import PropTypes from 'prop-types'
import { prop } from 'ramda'
import { sprintf } from 'sprintf-js'
import styled from 'styled-components'
import { useRouter } from 'next/router'
import { registryStatus } from 'constants/backend'
import * as ROUTES from 'constants/routes'
import dateFormat from 'helpers/dateFormat'
import { TableCol, TableRow } from 'components/Table'
import { TextOverflow } from 'components/StyledElements'

const Link = styled('div')`
  cursor: pointer;
  padding: 5px;
  transition: ${props => props.theme.transition};
  &:hover {
    color: ${props => props.theme.colors.primary};
  }
`

const Status = styled('div')`
  border-radius: ${props => props.theme.borderRadius};
  border: 1px solid;
  color: ${props => props.theme.colors[props.color]};
  display: inline-block;
  line-height: 16px;
  padding: 3px 12px;
`

const statusColors = {
  active: 'green',
  inactive: 'red',
  paused: 'yellow',
  extended: 'blue',
  expired: 'yellow'
}

const RegistryRow = props => {
  const { t, data, ...restProps } = props

  const router = useRouter()

  const area = prop('area', data)
  const number = prop('number', data)
  const titleYurdLisa = prop('title_yurd_lisa', data)
  const status = prop('status', data)
  const statusText = registryStatus.object[status]
  const statusColor = statusColors[status]
  const accreditationDate = dateFormat(prop('accreditation_date', data))

  const goToDetail = event => {
    event.stopPropagation()

    const path = ROUTES.REGISTRY_ITEM_PATH
    const asPath = sprintf(ROUTES.REGISTRY_ITEM_URL, area)
    return router.push(path, asPath)
  }

  return (
    <TableRow {...restProps}>
      <TableCol span={6}>
        <Link onClick={goToDetail}>
          {number}
        </Link>
      </TableCol>
      <TableCol span={6}>
        <TextOverflow limit={3}>
          {titleYurdLisa}
        </TextOverflow>
      </TableCol>
      <TableCol span={6}>
        {accreditationDate}
      </TableCol>
      <TableCol span={6}>
        <Status color={statusColor}>
          {t(statusText)}
        </Status>
      </TableCol>
    </TableRow>
  )
}

RegistryRow.propTypes = {
  t: PropTypes.func.isRequired,
  data: PropTypes.object,
}

export default RegistryRow

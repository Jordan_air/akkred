import React from 'react'
import PropTypes from 'prop-types'
import { prop } from 'ramda'
import styled from 'styled-components'
import dateFormat from 'helpers/dateFormat'
import getTranslate from 'helpers/getTranslate'
import {
  Table,
  TableBody,
  TableRow,
  TableCol
} from 'components/Table'
import { registryStatus } from '../../constants/backend'

const Wrapper = styled('div')``

const SimpleCol = styled(TableCol)`
  text-align: left;
  &:first-child {
    padding-left: 32px;
  }
  &:last-child {
    padding-right: 32px;
  }
`

const ExtLink = styled('a')`
  color: ${props => props.theme.colors.primary};
  text-decoration: none;
`

const statusColors = {
  active: 'green',
  inactive: 'red',
  paused: 'yellow',
  extended: 'blue',
  expired: 'yellow'
}

const Status = styled('div')`
  border-radius: ${props => props.theme.borderRadius};
  border: 1px solid;
  color: ${props => props.theme.colors[props.color]};
  display: inline-block;
  line-height: 16px;
  padding: 3px 12px;
`

const RegistryDetail = props => {
  const { t, data } = props

  const titleOrgan = prop('title_organ', data)
  const number = prop('number', data)
  const inn = prop('inn', data)
  const addressYurdLisa = prop('address_yurd_lisa', data)
  const phone = prop('phone', data)
  const email = prop('email', data)
  const webSite = prop('web_site', data)
  const fullNameSupervisorAo = prop('full_name_supervisor_ao', data)
  const addressOrgan = prop('address_organ', data)
  const fileOblast = prop('file_oblast', data)
  const certificate = prop('certificate', data)
  const isFileOblast = prop('is_file_oblast', data)
  const isCertificate = prop('is_certificate', data)
  const typeOrgan = getTranslate(prop('type_organ', data))
  const accreditationDate = dateFormat(prop('accreditation_date', data))
  const statusDate = dateFormat(prop('status_date', data))
  const status = prop('status', data)
  const standard = prop('designation_of_the_fundamental_standard', data)
  const statusText = registryStatus.object[status]
  const statusColor = statusColors[status]
  const accreditationDuration = dateFormat(prop('accreditation_duration', data))

  return (
    <Wrapper>
      <Table gutter={32}>
        <TableBody>

          <TableRow>
            <SimpleCol span={10}>{t('registry_table_number')}</SimpleCol>
            <SimpleCol span={14}>{number}</SimpleCol>
          </TableRow>
          <TableRow>
            <SimpleCol span={10}>{t('registry_status')}</SimpleCol>
            <SimpleCol span={14}> <Status color={statusColor}>
              {t(statusText)}
            </Status></SimpleCol>
          </TableRow>
          <TableRow>
            <SimpleCol span={10}>{t('registry_detail_legal_title')}</SimpleCol>
            <SimpleCol span={14}>{titleOrgan}</SimpleCol>
          </TableRow>
          <TableRow>
            <SimpleCol span={10}>{t('registry_detail_inn')}</SimpleCol>
            <SimpleCol span={14}>{inn}</SimpleCol>
          </TableRow>
          <TableRow>
            <SimpleCol span={10}>{t('registry_detail_legal_address')}</SimpleCol>
            <SimpleCol span={14}>{addressYurdLisa}</SimpleCol>
          </TableRow>
          <TableRow>
            <SimpleCol span={10}>{t('registry_detail_phone')}</SimpleCol>
            <SimpleCol span={14}>{phone}</SimpleCol>
          </TableRow>
          <TableRow>
            <SimpleCol span={10}>{t('registry_detail_email')}</SimpleCol>
            <SimpleCol span={14}>{email}</SimpleCol>
          </TableRow>
          <TableRow>
            <SimpleCol span={10}>{t('registry_detail_website')}</SimpleCol>
            <SimpleCol span={14}>{webSite}</SimpleCol>
          </TableRow>
          <TableRow>
            <SimpleCol span={10}>{t('registry_detail_supervisor_name')}</SimpleCol>
            <SimpleCol span={14}>{fullNameSupervisorAo}</SimpleCol>
          </TableRow>
          <TableRow>
            <SimpleCol span={10}>{t('registry_detail_accreditation_address')}</SimpleCol>
            <SimpleCol span={14}>{addressOrgan}</SimpleCol>
          </TableRow>
          <TableRow>
            <SimpleCol span={10}>{t('registry_detail_accreditation_date')}</SimpleCol>
            <SimpleCol span={14}>{accreditationDate}</SimpleCol>
          </TableRow>
          <TableRow>
            <SimpleCol span={10}>{t('registry_detail_accreditation_link')}</SimpleCol>
            <SimpleCol span={14}>
              {isFileOblast && (
                <ExtLink href={fileOblast} rel={'noopener noreferrer'} target={'_blank'}>
                  {t('download')}
                </ExtLink>
              )}
            </SimpleCol>
          </TableRow>

          <TableRow>
            <SimpleCol span={10}>{t('accreditation_certificate')}</SimpleCol>
            <SimpleCol span={14}>
              {isCertificate && (
                <ExtLink href={certificate} rel={'noopener noreferrer'} target={'_blank'}>
                  {t('download')}
                </ExtLink>
              )}

            </SimpleCol>
          </TableRow>

          <TableRow>
            <SimpleCol span={10}>{t('registry_detail_accreditation_type')}</SimpleCol>
            <SimpleCol span={14}>{typeOrgan}</SimpleCol>
          </TableRow>

          <TableRow>
            <SimpleCol span={10}>{t('registry_date_status')}</SimpleCol>
            <SimpleCol span={14}>с {statusDate}</SimpleCol>
          </TableRow>
          <TableRow>
            <SimpleCol span={10}>{t('registry_table_expiration')}</SimpleCol>
            <SimpleCol span={14}>{accreditationDuration}</SimpleCol>
          </TableRow>
          <TableRow>
            <SimpleCol span={10}>{t('registry_standard')}</SimpleCol>
            <SimpleCol span={14}>{standard}</SimpleCol>
          </TableRow>
        </TableBody>
      </Table>
    </Wrapper>
  )
}

RegistryDetail.propTypes = {
  t: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
}

export default RegistryDetail

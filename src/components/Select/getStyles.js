export default (theme, params) => ({
  control: (provided, state) => ({
    ...provided,
    backgroundColor: state.isFocused ? 'white' : theme.colors.grey,
    boxShadow: null,
    borderRadius: theme.borderRadius,
    borderColor: state.isFocused ? theme.colors.primary : 'transparent',
    transition: theme.transition,
    height: params.height,
    minHeight: params.height ? 'unset' : '36px',
    '&:hover': {
      backgroundColor: state.isFocused
        ? 'white'
        : theme.input.backgroundColorHover,
      borderColor: state.isFocused ? theme.colors.primary : 'transparent'
    }
  }),
  indicatorSeparator: () => ({}),
  loadingIndicator: (provided, state) => ({
    ...provided,
    '& span': {
      background: state.isFocused
        ? theme.input.placeholderColor
        : theme.input.placeholderColor
    }
  }),
  clearIndicator: provided => ({
    ...provided,
    alignItems: 'center',
    color: theme.input.placeholderColor,
    height: '100%',
    padding: '0 8px',
    '&:hover': {
      // color: theme.input.labelColor
    }
  }),
  dropdownIndicator: (provided, state) => ({
    ...provided,
    alignItems: 'center',
    color: state.isFocused ? theme.colors.primary : theme.input.placeholderColor,
    padding: '0 12px',
    transition: 'color 300ms, transform 150ms',
    transform: params.menuIsOpen ? 'rotate(180deg)' : 'rotate(0)',
    '&:hover': {
      // color: theme.input.labelColor
    }
  }),
  menuPortal: provided => ({
    ...provided,
    zIndex: 1500
  }),
  menu: provided => ({
    ...provided,
    border: theme.border,
    boxShadow: theme.boxShadow,
    borderRadius: theme.borderRadius,
    margin: '0',
    top: 'calc(100% + 4px)'
  }),
  menuList: provided => ({
    ...provided,
    padding: '7px'
  }),
  option: (provided, state) => ({
    ...provided,
    background: state.isSelected
      ? '#ebecfc'
      : state.isFocused
        ? '#f5f6fd'
        : 'none',
    borderRadius: theme.borderRadius,
    color: 'inherit',
    cursor: 'pointer',
    padding: '9px 12px',
    transition: theme.transition,
    '&:active': {
      background: '#f5f6fd'
    }
  }),
  valueContainer: (provided, state) => {
    const isMultiWithValues = state.hasValue && state.isMulti
    return {
      ...provided,
      padding: isMultiWithValues ? '4px' : '0 15px'
    }
  },
  singleValue: provided => ({
    ...provided,
    fontSize: '14px',
    color: 'inherit'
  }),
  placeholder: provided => ({
    ...provided,
    color: theme.input.placeholderColor,
    fontSize: '14px',
    margin: '0'
  }),
  noOptionsMessage: provided => ({
    ...provided,
    color: theme.input.placeholderColor
  }),
  loadingMessage: provided => ({
    ...provided,
    color: theme.input.placeholderColor
  })
})

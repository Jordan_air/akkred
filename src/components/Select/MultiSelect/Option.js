import React from 'react'
import PropTypes from 'prop-types'
import { components } from 'react-select'
import styled from 'styled-components'
import { Checkbox } from 'components/Toggle/Checkbox'

const StyledCheckbox = styled(Checkbox)`
  pointer-events: none;
  margin-left: ${props => props.isChild ? '16px' : 'unset'};
`

const Option = props => {
  const { value, label, isSelected } = props

  return (
    <components.Option {...props}>
      <StyledCheckbox
        value={value}
        label={label}
        checked={isSelected}
      />
    </components.Option>
  )
}

Option.propTypes = {
  value: PropTypes.any,
  label: PropTypes.any,
  isSelected: PropTypes.bool.isRequired,
  selectProps: PropTypes.object.isRequired,
  getValue: PropTypes.func.isRequired,
  setValue: PropTypes.func.isRequired,
  selectOption: PropTypes.func.isRequired,
  getStyles: PropTypes.func.isRequired,
}

export default Option

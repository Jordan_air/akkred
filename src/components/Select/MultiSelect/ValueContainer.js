import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { components } from 'react-select'
import { prop } from 'ramda'

const RSValueContainer = components.ValueContainer

const StyledContainer = styled(RSValueContainer)`
  padding: 4px 15px !important;
`

const More = styled('span')`
  font-size: 14px;
  margin-left: 5px;
`

const ValueContainer = props => {
  const { hasValue, children, ...restProps } = props

  const [values, input] = children

  const valuesCount = prop('length', values)

  if (hasValue) {
    return (
      <StyledContainer hasValue={hasValue} {...restProps}>
        {valuesCount > 0 && (
          <More>Выбрано {valuesCount}...</More>
        )}
        {input}
      </StyledContainer>
    )
  }

  return (
    <StyledContainer {...props} />
  )
}

ValueContainer.propTypes = {
  hasValue: PropTypes.bool,
  getStyles: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired,
}

export default ValueContainer

import React from 'react'
import PropTypes from 'prop-types'
import { not, isEmpty, equals, length } from 'ramda'
import { components } from 'react-select'
import { withTranslation } from 'hocs/withTranslation'
import { Checkbox } from 'components/Toggle/Checkbox'

const MenuList = props => {
  const {
    t,
    getValue,
    setValue,
    options,
    getStyles,
    theme
  } = props

  const selectedOptions = getValue()
  const hasOptions = not(isEmpty(options))

  const isSelectedAll = equals(length(selectedOptions), length(options))
  const isIndeterminate = not(isEmpty(selectedOptions)) && not(isSelectedAll)

  const onToggleSelectAll = (event) => {
    event.stopPropagation()
    if (isSelectedAll) {
      setValue([])
    } else {
      setValue(options)
    }
  }

  const customOptionParams = { theme, isSelected: isSelectedAll }
  const customOptionStyle = getStyles('option', customOptionParams)
  const checkboxStyle = { pointerEvents: 'none' }

  return (
    <components.MenuList {...props}>
      {hasOptions && (
        <div style={customOptionStyle} onClick={onToggleSelectAll}>
          <Checkbox
            style={checkboxStyle}
            label={t('select_all')}
            value={'select_all'}
            checked={isSelectedAll}
            indeterminate={isIndeterminate}
          />
        </div>
      )}
      {props.children}
    </components.MenuList>
  )
}

MenuList.propTypes = {
  t: PropTypes.func,
  children: PropTypes.node,
  getValue: PropTypes.func,
  setValue: PropTypes.func,
  options: PropTypes.array,
  getStyles: PropTypes.func,
  theme: PropTypes.object,
}

export default withTranslation()(MenuList)

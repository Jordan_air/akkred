import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { ChevronRight } from 'react-feather'

const Container = styled('div')`
  align-items: center;
  display: flex;
  flex-flow: row wrap;
  margin-bottom: 20px;
`

const Chevron = styled(ChevronRight)`
  margin-right: 5px;
  margin-left: 5px;
`

const Item = styled('div')`
  display: flex;
  flex-flow: row nowrap;
  align-items: center;
  margin-bottom: 5px;
  &:last-child:not(:first-child) {
    color: ${props => props.theme.colors.grey2}
  }
`

const Breadcrumb = props => {
  const children = React.Children.toArray(props.children)
  const childrenCount = React.Children.count(props.children)

  return (
    <Container>
      {children.map((child, key) => {
        const childCount = key + 1
        const childNode = React.cloneElement(child)
        return (
          <Item key={key}>
            {childNode}
            {childCount !== childrenCount &&
            <Chevron size={14} />}
          </Item>
        )
      })}
    </Container>
  )
}

Breadcrumb.propTypes = {
  children: PropTypes.node.isRequired
}

export default Breadcrumb

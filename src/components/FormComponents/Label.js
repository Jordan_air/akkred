import styled, { css } from 'styled-components'

export default styled('div')`
  color: black;
  font-size: 14px;
  font-weight: 500;
  line-height: 1.5;
  margin-bottom: 12px;
  ${props => props.error && (
    css`
      color: ${props => props.theme.colors.red} !important;
    `
  )}
  &:empty {
    display: none;
  }
`

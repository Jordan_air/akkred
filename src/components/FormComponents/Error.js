import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import hexToRgb from 'helpers/hexToRgb'

const Error = styled('div')`
  background-color: ${props => hexToRgb(props.theme.colors.red, '0.1')};
  border-radius: ${props => props.theme.borderRadius};
  color: ${props => props.theme.colors.red};
  font-size: 13px;
  margin-top: 8px;
  padding: 6px 20px;
  &:empty {
    display: none;
  }
`

const InputError = props => {
  return (
    <Error dangerouslySetInnerHTML={{ __html: props.children }} />
  )
}

InputError.propTypes = {
  children: PropTypes.string
}

export default InputError

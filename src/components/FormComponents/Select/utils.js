import { curry, equals, find, path, pipe, prop, propEq, propOr } from 'ramda'
import { getFilteredParams } from 'helpers/fetchData'
import toSnakeCase from 'helpers/toSnakeCase'
import { paramsToQueryString } from 'helpers/url'

const getPayloadFromSuccess = response => {
  const status = prop('status', response)
  if (equals(status, 200)) {
    return response.json()
  }
}

export const getOptions = ({ api, search, params, pageSize = 100 }) => {
  const allParams = { pageSize, search, ...params }
  const apiParams = pipe(getFilteredParams, toSnakeCase)(allParams)
  const query = paramsToQueryString(apiParams)

  return fetch(api + query)
    .then(getPayloadFromSuccess)
    .then(propOr([], 'results'))
}

export const getOption = api => id => {
  return fetch(`${api}${id}/`)
    .then(getPayloadFromSuccess)
}

export const getStaticOptions = (search, list) => Promise.resolve(list)
export const getStaticOption = (id, list) => Promise.resolve(find(propEq('id', id))(list))

export const defaultGetText = curry((text, obj) => path(text, obj))
export const defaultGetStaticText = curry((text, t, obj) => pipe(path(text), t)(obj))
export const defaultGetValue = curry((value, obj) => path(value, obj))

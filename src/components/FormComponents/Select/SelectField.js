import React, { Fragment, useEffect, useReducer } from 'react'
import PropTypes from 'prop-types'
import { find, prop, propEq, is, unionWith, eqBy } from 'ramda'
import reducer from 'helpers/reducer'
import { getFieldError } from 'helpers/form'
import { useDebounce } from 'hooks'
import Select, { MultiSelect } from 'components/Select'
import Label from '../Label'
import Error from '../Error'

const initialState = {
  options: [],
  loading: false,
  query: '',
  dirty: false
}

const getIdFromValue = inputValue => {
  if (is(Object, inputValue)) {
    return prop('id', inputValue)
  }
  return inputValue
}

const getSelectedOption = (options, value, isStatic) => {
  const optionId = isStatic
    ? prop('id', value) || value
    : parseInt(prop('id', value) || value)
  const option = find(propEq('id', optionId))(options)
  return option || ''
}

const onFetchData = (props, state, dispatch) => {
  const { getOptions, getValue, getText } = props

  dispatch({ loading: true, dirty: true })
  getOptions(state.query)
    .then(data => {
      const options = data.map(item => {
        const id = getValue(item)
        const name = getText(item)
        return { ...item, id, name }
      })

      dispatch({ options, loading: false })
    })
}

const SelectField = props => {
  const {
    input,
    meta,
    label,
    disabled,
    getOptions,
    getOption,
    getValue,
    getText,
    isStatic,
    isMulti,
    ...restProps
  } = props

  const [state, dispatch] = useReducer(reducer, initialState)

  const error = getFieldError(meta)
  const inputValueId = getIdFromValue(input.value)
  const selectedOption = getSelectedOption(state.options, input.value, isStatic)
  const debouncedQuery = useDebounce(state.query, 500)

  const onMenuOpen = () => {
    if (!state.dirty) {
      onFetchData(props, state, dispatch)
    }
  }

  useEffect(() => {
    if (debouncedQuery) {
      onFetchData(props, state, dispatch)
    }
  }, [debouncedQuery])

  useEffect(() => {
    if (inputValueId) {
      dispatch({ loading: true })
      getOption(inputValueId)
        .then(item => {
          const option = {
            id: getValue(item),
            name: getText(item)
          }
          const options = unionWith(
            eqBy(prop('id')),
            state.options,
            [option]
          )

          dispatch({ options, loading: false })
        })
    }
  }, [inputValueId])

  const onInputChange = (query, { action }) => {
    if (action === 'input-change') {
      if (!state.dirty) {
        dispatch({ dirty: true })
      }
      dispatch({ query })
    }
  }

  const customFilterOption = (option, rawInput) => {
    if (isStatic) {
      const words = rawInput.split(' ')
      const reducer = (acc, cur) => {
        const label = prop('label', option)
        return (acc && label) && label.toLowerCase().includes(cur.toLowerCase())
      }
      return words.reduce(reducer, true)
    }
    return true
  }

  const selectDefaultProps = {
    ...input,
    ...restProps,
    label,
    error,
    options: state.options,
    isLoading: state.loading,
    isClearable: true,
    getOptionLabel: prop('name'),
    getOptionValue: prop('id'),
    onMenuOpen,
    onInputChange,
    filterOption: customFilterOption
  }

  if (isMulti) {
    return (
      <Fragment>
        <Label error={error}>{label}</Label>
        <MultiSelect {...selectDefaultProps} />
        <Error>{error}</Error>
      </Fragment>
    )
  }

  return (
    <Fragment>
      <Label error={error}>{label}</Label>
      <Select
        {...selectDefaultProps}
        value={selectedOption}
      />
      <Error>{error}</Error>
    </Fragment>
  )
}

SelectField.propTypes = {
  input: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
  label: PropTypes.string,
  disabled: PropTypes.bool,
  getOptions: PropTypes.func.isRequired,
  getOption: PropTypes.func.isRequired,
  getValue: PropTypes.func.isRequired,
  getText: PropTypes.func.isRequired,
  isStatic: PropTypes.bool,
  isMulti: PropTypes.bool
}

export default SelectField

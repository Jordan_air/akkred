import React from 'react'
import PropTypes from 'prop-types'
import {
  getStaticOption,
  getStaticOptions,
  defaultGetStaticText,
  defaultGetValue
} from './utils'
import { withTranslation } from 'hocs/withTranslation'
import SelectField from './SelectField'

const UniversalStaticSelectField = props => {
  const { t, list, itemText } = props

  return (
    <SelectField
      getText={defaultGetStaticText(itemText, t)}
      getValue={defaultGetValue(['id'])}
      getOptions={search => getStaticOptions(search, list)}
      getOption={id => getStaticOption(id, list)}
      isStatic={true}
      {...props}
    />
  )
}

UniversalStaticSelectField.propTypes = {
  t: PropTypes.func.isRequired,
  list: PropTypes.array.isRequired,
  itemText: PropTypes.array
}

UniversalStaticSelectField.defaultProps = {
  itemText: ['title']
}

export default withTranslation()(UniversalStaticSelectField)

import React from 'react'
import PropTypes from 'prop-types'
import { IconInput } from 'components/Input'

const IconInputField = props => {
  const { input, ...restProps } = props

  return (
    <IconInput {...input} {...restProps} />
  )
}

IconInputField.propTypes = {
  input: PropTypes.object.isRequired,
}

export default IconInputField

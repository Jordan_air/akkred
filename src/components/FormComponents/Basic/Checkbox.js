import React from 'react'
import PropTypes from 'prop-types'
import { prop } from 'ramda'
import { Checkbox as CheckboxUI } from 'components/Toggle'

const Checkbox = props => {
  const { input, ...restProps } = props
  const checked = Boolean(prop('value', input))

  return (
    <CheckboxUI
      {...input}
      {...restProps}
      checked={checked}
      value={checked}
    />
  )
}

Checkbox.propTypes = {
  input: PropTypes.object
}

export default Checkbox

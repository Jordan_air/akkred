import React, { useContext } from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'
import { Row } from 'components/Grid'
import TableContext from './TableContext'

const StyledRow = styled(Row)`
  background-color: white;
  border-radius: 5px;
  font-size: 14px;
  min-height: 30px;
  
  ${props => props.isBody && (
    css`
      min-height: 60px;
      padding: 10px 0;
      &:nth-child(odd) {
        background-color: ${props => props.theme.colors.grey};
      }
    `
  )}
`

const TableRow = props => {
  const { children, isBody, ...restProps } = props

  const tableData = useContext(TableContext)
  const { gutter } = tableData

  const mapCols = (child, key) => {
    return React.cloneElement(child, { key, isBody })
  }

  return (
    <StyledRow
      align={'center'}
      gutter={gutter}
      isBody={isBody}
      isTable={true}
      {...restProps}>
      {React.Children.map(children, mapCols)}
    </StyledRow>
  )
}

TableRow.propTypes = {
  children: PropTypes.node,
  isBody: PropTypes.bool,
}

TableRow.defaultProps = {
  isBody: false
}

export default TableRow

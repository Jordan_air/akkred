import styled from 'styled-components'

export default styled('h1')`
  font-size: 24px;
  font-weight: 500;
  margin: 0 0 30px;
`

import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Link from 'components/Link'
import Title from './Title'

const Wrapper = styled('div')`
  align-items: center;
  display: flex;
  justify-content: space-between;
  margin-bottom: 30px;
`

const StyledTitle = styled(Title)`
  font-size: 30px;
  margin-bottom: 0;
`

const StyledLink = styled(Link)`
  font-size: 15px;
  font-weight: normal;
`

const TitleMore = props => {
  const { title, linkText, href } = props

  return (
    <Wrapper>
      <StyledTitle>{title}</StyledTitle>
      <StyledLink href={href}>{linkText}</StyledLink>
    </Wrapper>
  )
}

TitleMore.propTypes = {
  title: PropTypes.string.isRequired,
  linkText: PropTypes.string.isRequired,
  href: PropTypes.string.isRequired,
}

export default TitleMore

import React from 'react'
import PropTypes from 'prop-types'
import { Label, Error } from 'components/FormComponents'
import InputWrapper from './InputWrapper'
import InputBase from './InputBase'

const Input = props => {
  const { label, error, ...restProps } = props

  return (
    <InputWrapper>
      <Label error={error}>{label}</Label>
      <InputBase {...restProps} />
      <Error>{error}</Error>
    </InputWrapper>
  )
}

Input.propTypes = {
  label: PropTypes.string,
  error: PropTypes.string,
  placeholder: PropTypes.any,
  onChange: PropTypes.func,
  value: PropTypes.any,
}

Input.defaultProps = {
  type: 'text'
}

export default Input

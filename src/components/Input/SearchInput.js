import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Search } from 'react-feather'

const Input = styled('input')`
  background: ${(props) => props.theme.input.backgroundColor};
  border-radius: 6px 0px 0px 6px;
  border: ${(props) => props.theme.border};
  color: inherit;
  font-size: 14px;
  font-family: inherit;
  outline: none;
  height: 36px;
  padding: 0 15px;
  transition: ${(props) => props.theme.transition};
  width: 100%;

  ::placeholder {
    color: ${(props) => props.theme.input.placeholderColor};
  }
  ::-ms-input-placeholder {
    color: ${(props) => props.theme.input.placeholderColor};
  }

  :hover {
    background: ${(props) => props.theme.input.backgroundColorHover};
  }

  :focus {
    background: ${(props) => props.theme.input.backgroundColor};
    border-color: ${(props) => props.theme.colors.primary};
  }
`
const Label = styled('label')`
  display: flex;
  justify-content: center;
  align-items: center;
  background: blue;
  color: white;
  border-radius: 0px 6px 6px 0px;
  border: ${(props) => props.theme.border};
  font-size: 14px;
  outline: none;
  height: 36px;
  padding: 0 15px;
  transition: ${(props) => props.theme.transition};

  :focus {
    background: ${(props) => props.theme.input.backgroundColor};
    border-color: ${(props) => props.theme.colors.primary};
  }
`

const SearchInput = (props) => {
  const { setIsOpen, onKeyPress, onEnter, ...restProps } = props

  const onPress = (event) => {
    if (typeof onEnter === 'function' && event.key === 'Enter') {
      return onEnter(event, event.target.value)
    } else if (typeof onKeyPress === 'function') {
      return onKeyPress(event, event.target.value)
    }
    return null
  }

  return (
    <>
      <Input {...restProps} onKeyPress={onPress} id="search" />
      <Label onClick={() => setIsOpen(false)} for="search">
        <Search size={14} />
      </Label>
    </>
  )
}

SearchInput.propTypes = {
  onKeyPress: PropTypes.func,
  onEnter: PropTypes.func,
}

export default SearchInput

import { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { path } from 'ramda'
import { useTranslation } from 'components/I18N'
import { withTranslation } from 'hocs/withTranslation'

const TranslationWrapper = props => {
  const { children, i18n } = props

  const ns = path(['options', 'ns'], i18n)
  const { ready } = useTranslation(ns)

  const [isReady, setIsReady] = useState(false)

  useEffect(() => { setIsReady(ready) }, [ready])

  if (isReady) {
    return children
  }

  return 'Loading Translation'
}

TranslationWrapper.propTypes = {
  tReady: PropTypes.bool.isRequired,
  children: PropTypes.node.isRequired,
}

export default withTranslation()(TranslationWrapper)

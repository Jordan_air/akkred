export default [
  {
    title: 'Title 1',
    description: 'Lorem ipsum dolor sit amet.',
    imgUrl: '/pic01.jpg'
  },
  {
    title: 'Title 2',
    description: 'Lorem ipsum dolor sit amet.',
    imgUrl: '/pic02.jpg'
  },
  {
    title: 'Title 3',
    description: 'Lorem ipsum dolor sit amet.',
    imgUrl: '/pic03.jpg'
  },
  {
    title: 'Title 4',
    description: 'Lorem ipsum dolor sit amet.',
    imgUrl: '/pic04.jpg'
  },
  {
    title: 'Title 5',
    description: 'Lorem ipsum dolor sit amet.',
    imgUrl: '/pic05.jpg'
  }
]

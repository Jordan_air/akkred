import React from 'react'
import PropTypes from 'prop-types'
import { prop } from 'ramda'
import styled, { css, keyframes } from 'styled-components'
import { useRouter } from 'next/router'
import { ChevronLeft, ChevronRight } from 'react-feather'
import { devices } from 'constants/mediaQueries'
import hexToRgb from 'helpers/hexToRgb'
import getTranslate from 'helpers/getTranslate'
import { withTranslation } from 'hocs/withTranslation'
import { BackgroundImage, TextOverflow } from 'components/StyledElements'
import { ButtonPrimary } from 'components/Button'
import useSlider from './useSlider'

const fadeInAnimate = keyframes`
  0% {
    opacity: 0;
    visibility: hidden;
  }
  100% {
    opacity: 1;
    visibility: visible;
  }
`

const Wrapper = styled('div')`
  margin-top: -30px;
  position: relative;
`

const Nav = styled('button')`
  align-items: center;
  background-color: ${hexToRgb('#ffffff', '0.6')};
  border-radius: 50%;
  border: none;
  cursor: pointer;
  display: flex;
  justify-content: center;
  height: 50px;
  padding: 0;
  position: absolute;
  top: 50%;
  outline: none;
  transform: translateY(-50%);
  transition: ${props => props.theme.transition};
  width: 50px;
  z-index: 1;
  &:hover {
    background-color: white;
  }
  ${props => props.position === 'left' && (
    css`
      left: 30px;
    `
  )}
  ${props => props.position === 'right' && (
    css`
      right: 30px;
    `
  )}
`

const Slides = styled('div')`
  border-radius: 5px;
  overflow: hidden;
`

const Slide = styled('div')`
  animation: ${fadeInAnimate} 1000ms forwards;
  display: ${props => props.isCurrent ? 'block' : 'none'};
`

const Content = styled('div')`
  background: rgba(0, 0, 0, 0.5);
  border-radius: 5px;
  display: none;
  padding: 10px 20px;
  width: 65%;
  @media (min-width: ${devices.tabletL}) {
    display: block;
  }
`

const Title = styled(TextOverflow)`
  color: white;
  font-size: 34px;
  margin-bottom: 25px;
`

const Description = styled('div')`
  font-size: 15px;
`

const Button = styled(ButtonPrimary)`
  background-color: ${props => hexToRgb(props.theme.colors.red, '0.8')};
  font-size: 16px;
  height: 50px;
  padding: 0 45px;
  text-transform: uppercase;
  &:hover {
    background-color: ${props => props.theme.colors.red};
  }
`

const Background = styled(BackgroundImage)`
  height: 400px;
  padding: 50px 100px;
  width: 100%;
`

const Indicators = styled('div')`
  align-items: center;
  background-color: ${hexToRgb('#000000', '0.15')};
  border-radius: 5px;
  display: flex;
  padding: 6px 12px;
  position: absolute;
  left: 50%;
  bottom: 20px;
  transform: translateX(-50%);
`

const Indicator = styled('div')`
  background-color: ${props => props.isCurrent ? '#fff' : '#a9acb3'};
  border-radius: 50%;
  height: 12px;
  transition: ${props => props.theme.transition};
  width: 12px;
  &:not(:last-child) {
    margin-right: 15px;
  }
`

const getPathnameFromUrl = url => {
  return url.replace(/^.*\/\/[^/]+/, '')
}

const Slider = props => {
  const { data, t } = props
  const { currentSlide, goPrevSlide, goNextSlide } = useSlider(data)

  const router = useRouter()

  return (
    <Wrapper>
      <Nav position={'left'} onClick={goPrevSlide}>
        <ChevronLeft />
      </Nav>
      <Nav position={'right'} onClick={goNextSlide}>
        <ChevronRight />
      </Nav>

      <Slides>
        {data.map((item, index) => {
          const title = getTranslate(item, 'title')
          const description = prop('description', item)
          const image = prop('image', item)
          const link = getPathnameFromUrl(prop('link', item))
          const isCurrent = currentSlide === index

          return (
            <Slide key={index} isCurrent={isCurrent}>
              <Background url={image}>
                <Content>
                  <Title limit={3}>{title}</Title>
                  <Description>{description}</Description>
                  <Button onClick={() => router.push(link)}>
                    {t('more')}
                  </Button>
                </Content>
              </Background>
            </Slide>
          )
        })}
      </Slides>

      <Indicators>
        {data.map((item, index) => {
          const isCurrent = currentSlide === index
          return (
            <Indicator
              key={index}
              isCurrent={isCurrent}
            />
          )
        })}
      </Indicators>
    </Wrapper>
  )
}

Slider.propTypes = {
  t: PropTypes.func.isRequired,
  data: PropTypes.array.isRequired,
}

export default withTranslation()(Slider)

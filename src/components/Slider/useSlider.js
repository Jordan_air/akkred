import { useEffect, useState } from 'react'

export default (slides) => {
  const [currentSlide, setCurrentSlide] = useState(0)
  const slidesLastIndex = slides.length - 1

  useEffect(() => {
    const interval = setInterval(() => {
      if (slidesLastIndex !== 0) {
        const nextSlideIndex = currentSlide === slidesLastIndex ? 0 : currentSlide + 1
        setCurrentSlide(nextSlideIndex)
      }
    }, 5000)

    return () => {
      clearInterval(interval)
    }
  }, [currentSlide, slidesLastIndex])

  const goPrevSlide = () => {
    if (currentSlide === 0) {
      setCurrentSlide(slidesLastIndex)
    } else {
      setCurrentSlide(currentSlide - 1)
    }
  }

  const goNextSlide = () => {
    if (currentSlide === slidesLastIndex) {
      setCurrentSlide(0)
    } else {
      setCurrentSlide(currentSlide + 1)
    }
  }

  return {
    currentSlide,
    goPrevSlide,
    goNextSlide
  }
}

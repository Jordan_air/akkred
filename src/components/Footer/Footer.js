import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { devices } from 'constants/mediaQueries'
import hexToRgb from 'helpers/hexToRgb'
import { withTranslation } from 'hocs/withTranslation'
import Container from 'components/Container'
import Logo from 'components/NavBar/Logo'

const Wrapper = styled('div')`
  background-color: ${props => hexToRgb(props.theme.colors.primary, '0.9')};
  color: white;
  padding: 50px 0;
`

const FooterTop = styled('div')`
  margin-bottom: 40px;
  @media (min-width: ${devices.tabletL}) {
    display: flex;
    margin-bottom: 80px;
  }
`

const LogoWrap = styled('div')`
  display: flex;
  justify-content: center;
  margin-bottom: 20px;
  @media (min-width: ${devices.tabletL}) {
    display: block;
    margin-bottom: unset;
  }
`

const FooterBot = styled('div')`
  @media (min-width: ${devices.tabletL}) {
    display: flex;
    justify-content: space-between;
  }
`

const Description = styled('div')`
  font-size: 16px;
  line-height: 20px;
  text-align: center;
  @media (min-width: ${devices.tabletL}) {
    margin-left: 30px;
    text-align: unset;
    width: 70%;
  }
`

const Text = styled('div')`
  line-height: 20px;
  text-align: center;
  &:last-child {
    margin-top: 15px;
  }
  @media (min-width: ${devices.tabletL}) {
    text-align: unset;
    width: 50%;
    &:last-child {
      text-align: right;
      margin-top: unset;
    }
  }
`
const Static = styled('div')`
  font-size: 14px;
  text-align: left;
  line-height: 20px;
  padding-left: 10px;
  `

const Footer = props => {
  const { t } = props

  const date = new Date()
  const year = date.getFullYear()
  const copyrightText = `© ${year}. ${t('footer_copyright_sign')}`

  return (
    <Wrapper>
      <Container>
        <FooterTop>
          <LogoWrap>
            <Logo isHeader={false} />
          </LogoWrap>
          <Description>
            {t('footer_description')}
          </Description>



          {/* <Static> */}

          {/*  <div> */}

          {/*    <div> */}
          {/*      <strong>Kirishlar:</strong> 2000000 */}
          {/*    </div> */}

          {/*    <div> */}
          {/*      <strong>Kirishlar bugun:</strong> 2000000 */}
          {/*    </div> */}

          {/*    <div> */}
          {/*      <strong>Tashriflar:</strong> 2000000 */}
          {/*    </div> */}

          {/*    <div> */}
          {/*      <strong>Tashriflar bugun:</strong> 2000000 */}
          {/*    </div> */}

          {/*  </div> */}

          {/* </Static> */}
        </FooterTop>

        <FooterBot>
          <Text>{t('footer_copyright')}</Text>
          <Text>{copyrightText}</Text>
        </FooterBot>

        <div>
          <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">
            <img alt="Creative Commons Attribution 4.0 International"
                 style={{
                   borderWidth:0
                 }}
                 src="https://i.creativecommons.org/l/by/4.0/80x15.png" />
          </a><br />
          Saytdagi barcha materiallardan quyidagi lisenziya bo‘yicha foydalanish mumkin:
          <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International</a>.

        </div>
      </Container>
    </Wrapper>

  )
}

Footer.propTypes = {
  t: PropTypes.func.isRequired,
}

export default withTranslation()(Footer)

module.exports = {
  'env': {
    'browser': true,
    'es6': true
  },
  'extends': [
    'standard',
    'eslint:recommended',
    'plugin:react/recommended',
    "plugin:import/errors",
    "plugin:import/warnings"
  ],
  'globals': {
    'Atomics': 'readonly',
    'React': 'writable',
    'SharedArrayBuffer': 'readonly'
  },
  "parser": "babel-eslint",
  'plugins': ['react', "promise", "import",],
  'rules': {
    "react/jsx-indent": [2, 2],
    "react/jsx-boolean-value": [2, "always"],
    "react/display-name": "off",
    "react/no-unescaped-entities": "off",
    "react/jsx-fragments": "off",
    "react/jsx-curly-brace-presence": "off",
    "key-spacing": "off",
    "jsx-quotes": [2, "prefer-double"],
    "max-len": [2, 120, 2],
    "object-curly-spacing": [2, "always"],
    "indent": [
      "error", 2,
      { "SwitchCase": 1 }
    ],
    "comma-dangle": "off",
    "import/newline-after-import": ["error"],
    "import/no-unresolved": "off",
    "import/no-extraneous-dependencies": ["error"],
    "no-console": "warn"
  },
  "settings": {
    "react": {
      "version": "detect"
    }
  }
}